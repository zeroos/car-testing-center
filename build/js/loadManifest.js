var loadManifest = [
    //Vendor
    "js/vendor/easeljs-0.6.0.min.js",
    "js/vendor/lodash.min.js",
    "js/vendor/backbone-min.js",
"./js/car-testing-center.min.js",
    //Images
    {src: "img/sprites.png", id: "spritesheet"},
    {src: "img/sprites.json", id: "spritesheet-data"},
    //Data
    {src: "data/cars.json", id: "carsData"}
];

var mapLoadManifest = [
    "maps/1/metadata.json",
    "maps/2/metadata.json",
    "maps/3/metadata.json",
    "maps/4/metadata.json",
    "maps/5/metadata.json"
];
