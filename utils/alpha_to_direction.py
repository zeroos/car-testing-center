"""
    Adds alpha value for each solid pixel with an angle to the closest non-solid
    pixel
"""


import Image
import math


wall = (255, 0, 0)
wall_ignored = (160, 0, 0)
img = Image.open("./out3095.png")
pix = img.load()
angles = 255

def is_wall(point):
    return (point[0] == wall[0] and \
            point[1] == wall[1] and \
            point[2] == wall[2])

def is_ignored_wall(point):
    return (point[0] == wall_ignored[0] and \
            point[1] == wall_ignored[1] and \
            point[2] == wall_ignored[2])

def rad2myRad(rad):
    return int((rad*255) / (2*math.pi))

for (x, y) in [(x, y) for x in range(img.size[0]) for y in range(img.size[1])]:
    print x,y
    print int(10000*float(x*img.size[1] + y) / (img.size[0]*img.size[1]))/100.0, '%'
    if x % 5 == 0 and y == 0 and x>2930:
        print ("generating out%d.png..." % x ),
        img.save("out%d.png" % x)
        print "done"
    if not is_wall(pix[x,y]) or pix[x,y][3] != 255:
        continue
    r = 0
    found = False
    while not found:
        for angleFactor in [(255/angles)*i for i in range(0,angles)]:
            angle = (angleFactor/255.0) * 2 * math.pi
            px = r * math.cos(angle) + x
            py = r * math.sin(angle) + y
            if px < 0 or py < 0 or px >= img.size[0] or py >= img.size[1]:
                continue
            if not is_wall(pix[px, py]) and not is_ignored_wall(pix[px, py]):
                for r2 in range(r):
                    ppx = r2 * math.cos(angle) + x
                    ppy = r2 * math.sin(angle) + y
                    pix[ppx,ppy] = (pix[ppx,ppy][0], pix[ppx,ppy][1], pix[ppx,ppy][2],
                        rad2myRad(angle))
                found = True
                break
        r += 1
img.save("out.png")
