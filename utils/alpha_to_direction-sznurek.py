"""
    Adds alpha value for each solid pixel with an angle to the closest non-solid
    pixel

    THIS VERSION IS NOT WORKING, but the code is better than in the original one
"""


import Image
import math
import sys

wall = (255, 0, 0)
wall_ignored = (160, 0, 0)
img = Image.open(sys.argv[1])
pix = img.load()
angles = 255

def is_wall(point):
    return (point[0] == wall[0] and \
            point[1] == wall[1] and \
            point[2] == wall[2])

def is_ignored_wall(point):
    return (point[0] == wall_ignored[0] and \
            point[1] == wall_ignored[1] and \
            point[2] == wall_ignored[2])

def rad2myRad(rad):
    return int((rad*255) / (2*math.pi))

factors = [(255/angles)*i for i in xrange(0,angles)]
preangles = [(af / 255.0) * 2 * math.pi for af in factors]

def find_spot_angle(x, y, r, angle):
    px = r * math.cos(angle) + x
    py = r * math.sin(angle) + y
    if px < 0 or py < 0 or px >= img.size[0] or py >= img.size[1]:
        return False
    if not is_wall(pix[px, py]) and not is_ignored_wall(pix[px, py]):
        return True
    return False

def find_spot(x, y, r):
    for angle in preangles:
        if find_spot_angle(x, y, r, angle):
            return angle
    return None

def main():
    for (x, y) in ((x, y) for x in xrange(img.size[0]) for y in xrange(img.size[1])):
        print x, y
        if y == 0:
            print int(10000*float(x*img.size[1] + y) / (img.size[0]*img.size[1]))/100.0, '%'
        pixel = pix[x, y]
        if not is_wall(pixel) or pixel[3] != 255:
            continue

        r = 0
        step_size = 8
        while not find_spot(x, y, r):
            r += step_size
            if r > 5000:
                r = 0
                step_size /= 2
                continue

        angle = find_spot(x, y, r)
        while find_spot_angle(x, y, r, angle):
            r -= 1
        r += 1

        for r2 in range(r):
            ppx = r2 * math.cos(angle) + x
            ppy = r2 * math.sin(angle) + y
            pix[ppx,ppy] = (pix[ppx,ppy][0], pix[ppx,ppy][1], pix[ppx,ppy][2],
                         rad2myRad(angle))

    img.save("out.png")

main()
