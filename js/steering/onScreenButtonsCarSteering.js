var OnScreenButtonsCarSteering = function(car){
    this.car = car;
    this.handleMouseMove = _.throttle(_.bind(this._handleMouseMove, this), 100);
    this.handleTouchMove = _.throttle(_.bind(this._handleTouchMove, this), 100);
    this.handleTouchStart = _.bind(this._handleTouchStart, this);
    this.handleTouchEnd = _.bind(this._handlTouchEnd, this);
    this.handleMouseDown = _.bind(this._handleMouseDown, this);
    this.handleMouseUp = _.bind(this._handleMouseUp, this);

    this.brakePedalCoordinates = [0,0,0,0];
    this.brakePedalDown = false;
    this.braking = false;
};
_.extend(OnScreenButtonsCarSteering.prototype, {
    attach: function(){
        document.addEventListener('mousedown', this.handleMouseDown);
        document.addEventListener('mouseup', this.handleMouseUp);
        document.addEventListener('mousemove', this.handleMouseMove);
        document.addEventListener('touchmove', this.handleTouchMove);
        document.addEventListener('touchstart', this.handleTouchStart);
        document.addEventListener('touchend', this.handleTouchEnd);
    },
    detach: function(){
        document.removeEventListener('mousedown', this.handleMouseDown);
        document.removeEventListener('mouseup', this.handleMouseUp);
        document.removeEventListener('mousemove', this.handleMouseMove);
        document.removeEventListener('touchmove', this.handleTouchMove);
        document.removeEventListener('touchstart', this.handleTouchStart);
        document.removeEventListener('touchend', this.handleTouchEnd);
    },
    _handleMouseDown: function(ev){
        this.pointerDown(ev.clientX, ev.clientY);
    },
    _handleMouseUp: function(ev){
        this.pointerUp(ev.clientX, ev.clientY);
    },
    _handleMouseMove: function(ev){
        ev.preventDefault();
        this.pointerMoved(ev.clientX, ev.clientY);
    },
    _handleTouchMove: function(ev){
        for(var _i in ev.touches){
            var touch = ev.touches[_i];
            this.pointerMoved(touch.clientX, touch.clientY);
        }
    },
    _handleTouchStart: function(ev){
        ev.preventDefault();
        for(var _i in ev.touches){
            var touch = ev.touches[_i];
            this.pointerDown(touch.clientX, touch.clientY);
        }
    },
    _handleTouchEnd: function(ev){
        for(var _i in ev.touches){
            var touch = ev.touches[_i];
            this.pointerUp(touch.clientX, touch.clientY);
        }
    },
    pointerDown: function(x,y){
        this.braking = true;
        this.pointerMoved(x,y);
    },
    pointerUp: function(x,y){
        this.car.setBrakes(0);
        this.brakePedalDown = false;
        this.braking = false;
    },
    pointerMoved: function(x,y){
        if(!this.braking) return;
        var p = this.brakePedalCoordinates;
        if(x > p[0] && x < p[0]+p[2] && y>p[1] && y<p[1]+p[3]){
            this.car.setBrakes(1);
            this.brakePedalDown = true;
        }else{
            this.car.setBrakes(0);
            this.brakePedalDown = false;
        }
    },
    updateBrakePedalCoordinates: function(x,y,w,h){
        this.brakePedalCoordinates = [x,y,w,h];
    }
});
