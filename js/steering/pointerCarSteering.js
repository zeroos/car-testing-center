var PointerCarSteering = function(car, viewport){
    this.car = car;
    this.viewport = viewport;
    this.handleMouseMove = _.throttle(_.bind(this._handleMouseMove, this), 100);
    this.handleTouchMove = _.throttle(_.bind(this._handleTouchMove, this), 100);
    this.handleTouchStart = _.bind(this._handleTouchStart, this);
    this.handleContextMenu = _.bind(this._handleContextMenu, this);
    this.handleMouseDown = _.bind(this._handleMouseDown, this);
    this.handleMouseUp = _.bind(this._handleMouseUp, this);
    this.pointerX = undefined;
    this.pointerY = undefined;
    this.braking = false;
};
_.extend(PointerCarSteering.prototype, {
    attach: function(car, viewport){
        if(car !== undefined) this.car=car;
        if(viewport !== undefined) this.viewport = viewport;
        document.addEventListener('mousedown', this.handleMouseDown);
        document.addEventListener('mouseup', this.handleMouseUp);
        document.addEventListener('mousemove', this.handleMouseMove);
        document.addEventListener('touchmove', this.handleTouchMove);
        document.addEventListener('touchstart', this.handleTouchStart);
        document.addEventListener('contextmenu', this.handleContextMenu);
    },
    detach: function(){
        this.car = null;
        this.viewport = null;
        document.removeEventListener('mousedown', this.handleMouseDown);
        document.removeEventListener('mouseup', this.handleMouseUp);
        document.removeEventListener('mousemove', this.handleMouseMove);
        document.removeEventListener('touchmove', this.handleTouchMove);
        document.removeEventListener('touchstart', this.handleTouchStart);
        document.removeEventListener('contextmenu', this.handleContextMenu);
    },
    _handleMouseDown: function(ev){
        this.startBraking();
    },
    _handleMouseUp: function(ev){
        this.stopBraking();
    },
    _handleMouseMove: function(ev){
        ev.preventDefault();
        this.pointerMoved(ev.clientX, ev.clientY);
    },
    _handleTouchMove: function(ev){
        ev.preventDefault();
        this.pointerMoved(ev.touches[0].clientX, ev.touches[0].clientY);
    },
    _handleTouchStart: function(ev){
        ev.preventDefault();
    },
    _handleContextMenu: function(ev){
        ev.preventDefault();
    },
    startBraking: function(){
        this.braking = true;
    },
    stopBraking: function(){
        this.braking = false;
    },
    pointerMoved: function(x,y){
        this.pointerX = x;
        this.pointerY = y;
    },
    handleTick: function(fps){
        if(this.pointerX === undefined || this.pointerY === undefined) return;
        var carx = utils.m2px(this.car.x);
        var cary = utils.m2px(this.car.y);
        var x = this.pointerX;
        var y = this.pointerY;
        var vec = [
            x - carx + this.viewport.x,
            y - cary + this.viewport.y
        ];
        vec = utils.rotate(vec[0], vec[1], -this.car.r);

        var vecLength = utils.vectorLength(vec[0], vec[1]);

        if(this.braking){
            this.car.setBrakes(1);
        }
        if(vecLength > 200){
            if(this.braking){
                this.car.setAcceleration(0);
                this.car.setBrakes(1);
            }else{
                this.car.setAcceleration(1);
                this.car.setBrakes(0);
            }
        }else if(vecLength < 50){
            this.car.setAcceleration(0);
            //this.car.setBrakes(0);
        }else{
            if(this.braking){
                this.car.setAcceleration(0);
                this.car.setBrakes(1);
            }else{
                this.car.setAcceleration((vecLength-50)/150);
                this.car.setBrakes(0);
            }
        }

        if(vecLength > 30){
            if(vec[1]>0){
                this.car.setTurn(Math.atan(-vec[0]/vec[1]));
            }else{
                this.car.setTurn(Math.atan(vec[0]/vec[1]));
            }
        }else{
            this.car.setTurn(Math.atan(0));
        }

        if(this.car.getSpeed() < 3 && vec[1] < 0 && Math.atan(vec[0]/vec[1]) < Math.PI/4){
            this.car.setReverse(true);
        }
        if(vec[1] > 0 && this.car.reverse){
            this.car.setReverse(false);
        }
    }
});
