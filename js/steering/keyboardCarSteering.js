var KeyboardCarSteering = function(car){
    this.car = car;
    this.handleKeyDown = _.bind(this._handleKeyDown, this);
    this.handleKeyUp = _.bind(this._handleKeyUp, this);
};
_.extend(KeyboardCarSteering.prototype, {
    attach: function(){
        document.addEventListener('keydown', this.handleKeyDown);
        document.addEventListener('keyup', this.handleKeyUp);
    },
    detach: function(){
        document.removeEventListener('keydown', this.handleKeyDown);
        document.removeEventListener('keyup', this.handleKeyUp);
    },
    _handleKeyDown: function(ev){
        if(ev.keyCode == 38 || ev.keyCode == 87){//up
            this.car.setAcceleration(1);
        }else if(ev.keyCode == 40 || ev.keyCode == 83){//down
            this.car.setBrakes(1);
        }else if(ev.keyCode == 37 || ev.keyCode == 65){//left
            this.car.setTurn(-this.car.maxTurn);
        }else if(ev.keyCode == 39 || ev.keyCode == 68){//right
            this.car.setTurn(this.car.maxTurn);
        }
    },
    _handleKeyUp: function(ev){
        if(ev.keyCode == 38 || ev.keyCode == 87){//up
            this.car.setAcceleration(0);
        }else if(ev.keyCode == 40 || ev.keyCode == 83){//down
            this.car.setBrakes(0);
        }else if(ev.keyCode == 37 || ev.keyCode == 65){//left
            this.car.setTurn(0);
        }else if(ev.keyCode == 39 || ev.keyCode == 68){//right
            this.car.setTurn(0);
        }
    }
});
