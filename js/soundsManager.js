var SoundsManager = function(){

    this.audioSupported = false;
    if(typeof AudioContext !== "undefined"){
        this.ctx = new AudioContext();
    }else if(typeof webkitAudioContext !== "undefined"){
        this.ctx = new webkitAudioContext();
    }else{
        console.log("Audio not supported");
        return;
    }
    this.audioSupported = true;
    this.pluginInstalled = false;
    this.buffers = {};
    this.sources = [];
    this.waitingForBuffer = {};
    this.soundsGainNode = this.ctx.createGainNode();
    this.soundsGainNode.connect(this.ctx.destination);
    this.gainValue = 1;
};

_.extend(SoundsManager.prototype, {
    loadBuffer: function(url, id){
        if(!this.audioSupported) return;
        if(!this.pluginInstalled){
            window.loadQueue.addEventListener("fileload", _.bind(this.fileLoaded, this));
            this.pluginInstalled = true;
        }
        window.loadQueue.loadFile({"id":id, "src": url});
    },
    fileLoaded: function(ev){
        if(!this.audioSupported) return;
        if(ev.item.id in this.waitingForBuffer){
            this.ctx.decodeAudioData(ev.result, _.bind(function(buffer){
                this.buffers[ev.item.id] = buffer;
                for(var _i=0; _i<this.waitingForBuffer[ev.item.id].length; _i++){
                    this.waitingForBuffer[ev.item.id][_i]();
                }
                delete this.waitingForBuffer[ev.item.id];
            }, this),this.onError);
        }
    },
    onError: function(err){
        if(!this.audioSupported) return;
        console.log("Audio error", err);
    },
    getPreloadHandlers: function(){
    return {
            callback: _.bind(function(src, type, id){
                this.waitingForBuffer[id] = [];
                return {src: src, id:id, type:createjs.LoadQueue.BINARY};
            }, this),
            types: [createjs.LoadQueue.SOUND],
            extensions: []
        };
    },
    isBufferLoaded: function(bufferId){
        if(!this.audioSupported) return;
        return (bufferId in this.buffers);
    },
    getSource: function(bufferId, callback){
        if(!this.audioSupported) return;
        if(!this.isBufferLoaded(bufferId)){
            this.waitingForBuffer[bufferId].push(_.bind(function(){
                this.getSource(bufferId, callback);
            }, this));
            return false;
        }
        var source = this.ctx.createBufferSource();
        source.buffer = this.buffers[bufferId];
        source.connect(this.soundsGainNode);
        this.sources.push(source);
        if(callback !== undefined){
            callback(source);
        }
        return source;
    },
    muteSounds: function(){
        this.soundsGainNode.gain.value = 0;
    },
    unmuteSounds: function(){
        this.soundsGainNode.gain.value = this.gainValue;
    },
    toggleMute: function(){
        if(this.soundsGainNode.gain.value === 0){
            this.unmuteSounds();
        }else{
            this.muteSounds();
        }
    },
    setSoundsVolume: function(vol){
        this.gainValue = vol;
        this.soundsGainNode.gain.value = this.gainValue;
    }
});
