var ImageManager = function(spritesheet, data){
    this.spritesheet = spritesheet;
    this.data = data;
};

ImageManager.prototype.getSprite = function(name){
    if(this.spritesheet === undefined || this.data === undefined){
        console.error("Images not loaded");
    }
    var image = this.data[name];
    var result = {
        'img': this.spritesheet,
        'x': utils.stripSuffix(image.x, 'px'),
        'y': utils.stripSuffix(image.y, 'px'),
        'w': utils.stripSuffix(image.w, 'px'),
        'h': utils.stripSuffix(image.h, 'px')
    };
    result.rect = new createjs.Rectangle(
            result.x, 
            result.y, 
            result.w, 
            result.h
    );
    return result;
};

