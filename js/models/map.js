var MapModel = Model.extend({
    initialize: function(){
        this.track = null;
        this.checkpoints = [];
        this.objects = [];
    },
    getPixelData: function(x,y){
        x = Math.floor(utils.m2px(x));
        y = Math.floor(utils.m2px(y));
        var pixelData = this.colormap
                .getContext('2d')
                .getImageData(x, y, 1, 1)
                .data;
        return pixelData;
    },
    getColor: function(x,y){
        var pixelData = this.getPixelData(x,y);
        var rgb = (pixelData[0] << 16) + (pixelData[1] << 8) + (pixelData[2]);
        if(pixelData[3] === 0){
            return undefined;
        }
        return "#"+("000000" + rgb.toString(16)).substr(-6);
    },
    getData: function(x,y){
        var color = this.getColor(x,y);
        if(color === undefined) return undefined;
        return this.data.colormap[color];

    },
    setTrack: function(trackID){
        this.track = trackID;
        var translatedCheckpoints = [];
        var dataCheckpoints = this.getTrackData().checkpoints;
        for(var _i in dataCheckpoints){
            var checkpoint = dataCheckpoints[_i];
            var translatedCheckpoint = new Checkpoint({
                "x1": utils.px2m(checkpoint.x1),
                "y1": utils.px2m(checkpoint.y1),
                "x2": utils.px2m(checkpoint.x2),
                "y2": utils.px2m(checkpoint.y2)
            });
            if(checkpoint.speed_lt !== undefined){
                translatedCheckpoint.speed_lt = checkpoint.speed_lt;
            }
            if(checkpoint.speed_gt !== undefined){
                translatedCheckpoint.speed_gt = checkpoint.speed_gt;
            }
            if(_i === "0"){
                translatedCheckpoint.active = true;
            }
            if(checkpoint.display){
                translatedCheckpoint.display = true;
            }
            translatedCheckpoints.push(translatedCheckpoint);
        }
        this.checkpoints = translatedCheckpoints;

        var translatedObjects = [];
        var layout = this.getTrackData().layout;
        if(layout === undefined) return;
        var dataObjects = this.data.layouts[layout].objects;
        for(var i=0; i<dataObjects.length; i++){
            var obj = dataObjects[i];
            var tObj = {
                pos: [
                    utils.px2m(obj.pos[0]),
                    utils.px2m(obj.pos[1])
                ],
                name: obj.name
            };
            translatedObjects.push(tObj);
        }
        this.objects = translatedObjects;
    },
    isSolid: function(x,y){
        var color = this.getColor(x,y);
        if(color === undefined) return true;
        var data= this.data.colormap[color];
        if(data === undefined) return true;
        if(data.type === undefined) return false;
        if(data.type == "solid") return true;
        if(data.type == "solid-angle"){
            var pixelData = this.getPixelData(x,y);
            return {'angle': pixelData[3] * 2 * Math.PI / 255};
        }
        return false;
    },
    getSlipColor: function(x,y){
        var data = this.getData(x,y);
        if(data === undefined || data.slip === undefined) return "rgba(0,0,0,0.4)";
        return data.slip.color;    
    },
    getStaticFriction: function(x,y){
        var data = this.getData(x,y);
        if(data === undefined || data.staticFriction === undefined) return 1500;
        return data.staticFriction;
    },
    getKineticFriction: function(x,y){
        var data = this.getData(x,y);
        if(data === undefined || data.kineticFriction === undefined) return 900;
        return data.kineticFriction;
    },
    getDrag: function(x,y){
        var data = this.getData(x,y);
        if(data === undefined || data.drag === undefined) return 0;
        return data.drag;
    },
    getStart: function(){
        if(this.track === null){
            return this.data.start;
        }else{
            return this.getTrackData().start;
        }
    },
    getTrackData: function(){
        return this.data.tracks[this.track];
    },
    getWidth: function(){
        return utils.px2m(this.colormap.width);
    },
    getHeight: function(){
        return utils.px2m(this.colormap.height);
    }
});

MapModel.loadFromJSON = function(jsonData){
    var colormapCanvas = document.createElement('canvas');
    var texturedCanvas = document.createElement('canvas');

    var colormap = loadQueue.getResult("maps/"+jsonData.id+"/" + jsonData.files.colormap);
    var textured = loadQueue.getResult("maps/"+jsonData.id+"/" + jsonData.files.textured);

    var backgroundUrl = "maps/" + jsonData.id + "/" + jsonData.files.background;

    colormapCanvas.width = colormap.width;
    colormapCanvas.height = colormap.height;
    colormapCanvas.getContext('2d').drawImage(colormap, 0, 0, colormap.width, colormap.height);

    texturedCanvas.width = textured.width;
    texturedCanvas.height = textured.height;
    texturedCanvas.getContext('2d').drawImage(textured, 0, 0, textured.width, textured.height);
    return new MapModel({
        'colormap': colormapCanvas,
        'textured': texturedCanvas,
        'backgroundUrl': backgroundUrl,
        'data': jsonData
    });
};

MapModel.loadMap = function(mapId, callback){
    var loadResult = window.loadQueue.getResult("maps/"+mapId+"/metadata.json");
    var metadataLoaded = _.once(MapModel.genMetadataLoadedFunction(mapId, callback));
    if(loadResult === null || loadResult === undefined){
        window.loadQueue.addEventListener("complete", metadataLoaded);
    }else{
        metadataLoaded();
    }
};

MapModel.genMetadataLoadedFunction = function(mapId, callback){
    return function(){
        var mapData = window.loadQueue.getResult("maps/"+mapId+"/metadata.json");
        if(callback !== undefined){
            this.loadQueue.addEventListener("complete", _.once(function(){ callback(mapData);}));
        }
        loadQueue.loadManifest([
            "maps/"+mapId+"/"+mapData.files.colormap,
            "maps/"+mapId+"/"+mapData.files.background,
            "maps/"+mapId+"/"+mapData.files.textured
        ]);

    };
};
