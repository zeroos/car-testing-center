var Model = function(def, opts){
    this.reset(def, opts);
};


_.extend(Model.prototype, {
    defaults: function(){
        return {started: false};
    },
    set: function(dict){
        for(var _prop in dict){
            this[_prop] = dict[_prop];
        }
    },
    initialize: function(opts){
    },
    reset: function(data, opts){
        var def = this.defaults();
        _.extend(def, data);
        this.set(def);
        this.initialize(opts);
    },
    start: function(){
    }

});
Model.extend = Backbone.Model.extend;
