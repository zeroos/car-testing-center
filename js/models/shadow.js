var Shadow = Model.extend({
    defaults: function(){
        return {
            'startTime' : null,
            'x': 0,
            'y': 0,
            'r': 0,
            'image_normal': 'car_fores_black',
            'image_braking': 'car_fores_black-braking'
        };
    },
    initialize: function(){
        if(this.data === undefined){
            throw "No data for shadow given";
        }
    },
    handleTick: function(){
        var samples_f = this.getCurrentTime()/(1000/SHADOW_LOG_FREQ);
        var time = this.getCurrentTime();
        var sample0_num = Math.floor(samples_f);
        var sample1_num = sample0_num+1;
        if(this.data.length < sample1_num){
            return;
        }else if(this.data.length == sample1_num){
            sample1_num = sample0_num;
        }
        var sample0 = this.data[sample0_num];
        var sample1 = this.data[sample1_num];
        var t0 = sample0_num * SHADOW_LOG_FREQ;
        var t1 = sample1_num * SHADOW_LOG_FREQ;
        var dt1 = time/1000-t0;
        var dt2 = time/1000-t1;

        var x0 = sample0.x + ((sample0.vx + sample1.vx)*dt1/2);
        var y0 = sample0.y + ((sample0.vy + sample1.vy)*dt1/2);
        var r0 = sample0.r + ((sample0.vr + sample1.vr)*dt1/2);

        var x1 = sample1.x + ((sample0.vx + sample1.vx)*dt2/2);
        var y1 = sample1.y + ((sample0.vy + sample1.vy)*dt2/2);
        var r1 = sample1.r + ((sample0.vr + sample1.vr)*dt2/2);

        this.x = (x0+x1)/2;
        this.y = (y0+y1)/2;
        this.r = (r0+r1)/2;
        this.regX = sample0.regX;
        this.regY = sample0.regY;
    },
    getCurrentTime: function(){
        if(this.startTime === null) return 0;
        return (new Date()).getTime()  - this.startTime;
    },
    restart: function(){
        this.start();
    },
    start: function(){
        this.startTime = (new Date()).getTime();
    }
});
