var LapTimer = Model.extend({
    initialize: function(){
        this.startTime = null;
        this.lapStartTime = null;
        this.checkpoints = [];
        this.bestLapId = null;
        this.lapsHistory = [];
        this.checkpointsHistory = [];
        this.totalToLastLap = null;
        this.penealities = 0;
    },
    isStarted: function(){
        return this.startTime !== null;
    },
    start: function(){
        this.lapStartTime = this.getCurrentTime();
        this.startTime = this.lapStartTime;
    },
    getRaceTime: function(){
        //works only after the race is finished
        return this.totalToLastLap;
    },
    getCurrentRaceTime: function(){
        if(!this.isStarted()) return 0;
        return this.getCurrentTime() - this.startTime;
    },
    getCurrentLapTime: function(){
        if(!this.isStarted()) return 0;
        return this.getCurrentTime() - this.lapStartTime;
    },
    getCurrentTime: function(){
        return (new Date()).getTime();
    },
    getBestLapTime: function(){
        if(this.bestLapId === null) return 0;
        return this.lapsHistory[this.bestLapId];
    },
    getPreviousLapTime: function(){
        if(this.lapsHistory.length === 0) return 0;
        return this.lapsHistory[this.lapsHistory.length-1];
    },
    getBestLapCheckpoints: function(){
        if(this.bestLapId === null) return 0;
        return this.checkpointsHistory[this.bestLapId];
    },
    addPeneality: function(time){
        this.lapStartTime -= time*1000;
        this.startTime -= time*1000;
        this.penealities += time;
    },
    checkpointReached: function(){
        this.checkpoints.push(this.getCurrentLapTime());
    },
    lapFinished: function(){
        var lapTime = this.getCurrentLapTime();
        this.totalToLastLap = this.getCurrentRaceTime();
        this.lapsHistory.push(lapTime);
        this.checkpointsHistory.push(this.checkpoints);
        this.checkpoints = [];
        this.lapStartTime = this.getCurrentTime();
        if(this.bestLapId === null || this.getBestLapTime() > lapTime){
            this.bestLapId = this.lapsHistory.length-1;
        }
    },
    formatTime: function(time){
        var min = Math.floor(time/60000);
        var sec = Math.floor(time/1000)%60;
        var dsec = Math.floor(time/10)%100;
        if(sec < 10) sec = "0" + sec;
        if(dsec < 10) dsec = "0" + dsec;
        return min + ":" + sec + "." + dsec;
    }
});
