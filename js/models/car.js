var Car = RigidBody.extend({
    defaults: function() {
        var prev = RigidBody.prototype.defaults.apply(this);
        _.extend(prev, {
            'turn': 0,
            'acceleration': 0,
            'brakes': 0,
            'intendedTurn': 0,
            'intendedAcceleration': 0,
            'turnChangeSpeed': Math.PI/45,
            'maxTurn': Math.PI/4,
            'maxAcceleration': 2000,
            'maxBrakes': 1500,
            'accelerationChangeSpeed': undefined,
            'brakesChangeSpeed': undefined,
            'wheels': null,
            'slipping': false,
            'rpm': 0,
            'rpmChangeSpeedMin': -150,
            'rpmChangeSpeedMax': 250,
            'gear': 1,
            'reverse': false,
            'maxGear': 5,
            'minClutch': 0,
            'maxClutch': 1,
            'clutch': 1,
            'clutchChangeSpeed': 0.1,
            'intendedClutch': 1,
            'gearsFactors': [0.1,0.09,0.13,0.18,0.23,0.28],
            'checkpoint': 0,
            'lap': 0,
            'currentGraphics': null,
            'maxLap': -1,
            'timer': new LapTimer(),
            'finishCallback': null,
            'penealityCallback': null,
            'log': [],
            'penealitiesToDisplay': []//this should be moved to view somehow
        });
        return prev;
    },
    initialize: function(){
        RigidBody.prototype.initialize.apply(this);
        if(this.accelerationChangeSpeed === undefined){
            this.accelerationChangeSpeed = this.maxAcceleration/10;
        }

        if(this.brakesChangeSpeed === undefined){
            this.brakesChangeSpeed = this.maxBrakes/3;
        }
        if(this.wheels === null){
            var w = this.w;
            var h = this.h;
            var wheels = [
                new WheelModel({'x': w*2/5, 'y': h/3, 'steering': 1, 'accelerate': 0.75}),
                new WheelModel({'x': -w*2/5, 'y': h/3, 'steering': 1, 'accelerate': 0.75}),
                new WheelModel({'x': w*2/5, 'y': -h/3, 'accelerate': 0.5}),
                new WheelModel({'x': -w*2/5, 'y': -h/3, 'accelerate': 0.5})
            ];
            this.wheels = wheels;
        }else{
            var newWheels = [];
            for(var i=0; i<this.wheels.length; i++){
                var wheelData = this.wheels[i];
                newWheels.push(new WheelModel(wheelData));
            }
            this.wheels = newWheels;
        }
    },
    start: function(){
        this.timer.start();
    },
    setGraphics: function(g){
        this.currentGraphics = g;
    },
    getWheelPosition: function(wheel){
        return this.getPointPosition(wheel.x, wheel.y);
    },
    getAccelerationForce: function(){
        var rpm = this.rpm;
        var acceleration = this.acceleration;

        if(rpm <= 2000){
            acceleration = acceleration*2/5;
        }else if(rpm < 5000){
            acceleration = rpm*acceleration/5000;
        }else if(rpm < 8000){
            acceleration = (-rpm/10000 + 1.5)*acceleration;
        }else{
            acceleration = 0;
        }


        if(this.reverse){
            acceleration *= -1;
        }
        return [0, acceleration];
    },
    handleTick: function(fps){
        RigidBody.prototype.handleTick.apply(this,[fps]);

        if(this.intendedTurn < this.turn){
            this.turn -= this.turnChangeSpeed;
            if(this.intendedTurn > this.turn){
                this.turn = this.intendedTurn;
            }
        }else if(this.intendedTurn > this.turn){
            this.turn += this.turnChangeSpeed;
            if(this.intendedTurn < this.turn){
                this.turn = this.intendedTurn;
            }
        }

        if(this.intendedAcceleration < this.acceleration){
            this.acceleration -= this.accelerationChangeSpeed;
            if(this.intendedAcceleration > this.acceleration){
                this.acceleration = this.intendedAcceleration;
            }
        }else if(this.intendedAcceleration > this.acceleration){
            this.acceleration += this.accelerationChangeSpeed;
            if(this.intendedAcceleration < this.acceleration){
                this.acceleration = this.intendedAcceleration;
            }
        }

        if(this.intendedBrakes < this.brakes){
            this.brakes -= this.brakesChangeSpeed;
            if(this.intendedBrakes > this.brakes){
                this.brakes = this.intendedBrakes;
            }
        }else if(this.intendedBrakes > this.brakes){
            this.brakes += this.brakesChangeSpeed;
            if(this.intendedBrakes < this.brakes){
                this.brakes = this.intendedBrakes;
            }
        }

        if(this.intendedClutch > this.maxClutch){
            this.intendedClutch = this.maxClutch;
        }else if(this.intendedClutch < this.minClutch){
            this.intendedClutch = this.minClutch;
        }

        if(this.intendedClutch < this.clutch){
            this.clutch -= this.clutchChangeSpeed;
            if(this.intendedClutch > this.clutch){
                this.clutch = this.intendedClutch;
            }
        }else if(this.intendedClutch > this.clutch){
            this.clutch += this.clutchChangeSpeed;
            if(this.intendedClutch < this.clutch){
                this.clutch = this.intendedClutch;
            }
        }
 
        
        var intendedRPM = this.getIntendedRPM();
        if(intendedRPM < this.rpm){
            this.rpm += this.rpmChangeSpeedMin;
            if(intendedRPM > this.rpm){
                this.rpm = intendedRPM;
            }
        }else if(intendedRPM > this.rpm){
            this.rpm += this.rpmChangeSpeedMax;
            if(intendedRPM < this.rpm){
                this.rpm = intendedRPM;
            }
        }

        //clutch
        if(this.acceleration === 0){
            this.intendedClutch = 1;
        }else if(this.rpm > 4000){
            this.intendedClutch = 1;
        }else if(this.rpm < 3000){
            this.intendedClutch = 0.5;
        }

        //gears
        if(!this.reverse){
            if(this.rpm > 6000 && this.gear < this.maxGear){
                this.gear++;
            }
            if(this.rpm < 3500 && this.gear > 1){
                this.gear--;
            }
        }
        if(this.rpm < 1400){
            this.rpm = 1500+30*Math.random();
        }

        this._applyWheelForces();
        this._lookForCheckpoint();
        this._log();
    },
    setAcceleration: function(acc){
        acc = this.maxAcceleration * acc;

        if(acc > this.maxAcceleration){
            acc = this.maxAcceleration;
        }else if(acc < 0){
            acc = 0;
        }
        this.intendedAcceleration = acc;
    },
    setTurn: function(rad){
        if(rad > this.maxTurn){
            rad = this.maxTurn;
        }else if(rad < -this.maxTurn){
            rad = -this.maxTurn;
        }
        this.intendedTurn = rad;
    },
    setReverse: function(reverse){
        if(reverse){
            this.gear = 0;
        }else if(this.reverse){
            this.gear = 1;
        }
        this.reverse = reverse;
    },
    getMaxDrivingWheelRPM: function(){
        var maxRPM = 0;
        for(var _i in this.wheels){
            var wheel = this.wheels[_i];
            if(wheel.accelerate === 0){
                continue;
            }
            var w = this.getSpeed()/(utils.inch2m(wheel.diameter)/2);
            var n =w*60/(2*Math.PI);
            if(n > maxRPM) maxRPM = n;
        }
        return maxRPM;
    },
    getIntendedRPM: function(){
        var rpm = this.getMaxDrivingWheelRPM();
        rpm /= this.clutch;
        return rpm/this.gearsFactors[this.gear];
    },
    setBrakes: function(brakes){
        brakes = this.maxBrakes * brakes;

        if(brakes > this.maxBrakes){
            brakes = this.maxBrakes;
        }else if(brakes < 0){
            brakes = 0;
        }
        this.intendedBrakes = brakes;
    },
    _lookForCheckpoint: function() {
        var nextCheckpoint = this.map.checkpoints[this.checkpoint];

        var dist = utils.pointToSegmentDistanceSquared(
                this.x, this.y,
                nextCheckpoint.x1, nextCheckpoint.y1,
                nextCheckpoint.x2, nextCheckpoint.y2
        );

        if(dist < 5){
            if(nextCheckpoint.speed_lt !== undefined){
                if(this.getSpeed() > nextCheckpoint.speed_lt) return;
            }
            if(nextCheckpoint.speed_gt !== undefined){
                if(this.getSpeed() < nextCheckpoint.speed_gt) return;
            }

            nextCheckpoint.active = false;

            this.checkpoint = (this.checkpoint+1)%this.map.checkpoints.length;
            this.timer.checkpointReached();


            nextCheckpoint = this.map.checkpoints[this.checkpoint];
            nextCheckpoint.active = true;

            if(this.checkpoint === 0){
                this.lap ++;
                this.timer.lapFinished();
                if(this.lap >= this.maxLap && this.finishCallback){
                    this.finishCallback(this.timer, this.log);
                }
            }
        }
    },
    _log: function(){
        var time = this.timer.getCurrentRaceTime()/(1000/SHADOW_LOG_FREQ); //sample is taken once per second
        time = Math.floor(time);
            if(this.log[time] === undefined){
            this.log[time] = {
                'x': this.x,
                'y': this.y,
                'r': this.r,
                'vx': this.vx,
                'vy': this.vy,
                'vr': this.vr,
                'regX': this.regX,
                'regY': this.regY
            };
        }

    },
    _applyWheelForces: function() {
        this.slipping = false;
        for(var _i in this.wheels){
            var wheel = this.wheels[_i];
            var pos = this.getWheelPosition(wheel);

            var steering = wheel.steering;
            if(steering !== 0){
                wheel.r = this.turn*steering;
            }

            var pointSpeed = this.getPointSpeed(wheel.x, wheel.y);
            var carInternalSpeed = this.getInternalSpeed();
            pointSpeed[0] += carInternalSpeed[0];
            pointSpeed[1] += carInternalSpeed[1];
            var t = 0.7; //time to brake [s] if we want to break instantly

            //perpendicular force
            var pVector = utils.rotate(0, 1, Math.PI/2+wheel.r);
            var pSpeed = utils.projectVector(pointSpeed[0], pointSpeed[1], pVector[0], pVector[1]);
            var pForce = [
                -(pSpeed[0]*this.m/4)/t, 
                -(pSpeed[1]*this.m/4)/t
            ];
            if(wheel.slipping){
                pForce = [0.9*pForce[0], 0.9*pForce[1]];
            }
        
            var bForce;
            //braking force
            if(this.brakes !== 0){
                //bForce = utils.rotate(0,-this.brakes, wheel.r);
                bForce = [0,-this.brakes];
                var bSpeed = utils.projectVector(pointSpeed[0], pointSpeed[1], bForce[0], bForce[1]);
                var maxBForce = [-(bSpeed[0]*this.m/4)/t, -(bSpeed[1]*this.m/4)/t];
                if(utils.vectorLength(maxBForce[0], maxBForce[1]) < this.brakes){
                    bForce = maxBForce;
                }
                bForce = maxBForce;
            }else{
                bForce = [0,0];
            }

            //acceleration force
            var accelerationFactor = wheel.accelerate;
            var accForce = [0,0];
            if(this.acceleration !== 0 && accelerationFactor !== 0){
                accForce = this.getAccelerationForce();
                accForce[1] = accForce[1]*accelerationFactor;
                accForce = utils.rotate(accForce[0],accForce[1], wheel.r);
            }


            //drag force
            var drag = this.map.getDrag(pos[0], pos[1]);
            var dragForce = [(-pointSpeed[0])*drag, 
                             (-pointSpeed[1])*drag];
            
            var totalForce = [
                pForce[0]+bForce[0]+accForce[0]+dragForce[0], 
                pForce[1]+bForce[1]+accForce[1]+dragForce[1]
            ];
                
            var staticFriction = wheel.staticFrictionFactor*this.map.getStaticFriction(pos[0], pos[1]);
            if(utils.vectorLength(totalForce[0], totalForce[1])>staticFriction){
                var kineticFriction = wheel.kineticFrictionFactor*this.map.getKineticFriction(pos[0], pos[1]);
                var factor = utils.vectorLength(totalForce[0], totalForce[1])/kineticFriction;
                
                totalForce = [totalForce[0]/factor, totalForce[1]/factor];
                wheel.slipping = true;
                this.slipping = true;
            }else if(wheel.slipping === true){
                wheel.slipping = false;
            }
            
            
            //apply force
            /*
            this.applyForce(
                wheel.x,
                wheel.y,
                dragForce[0],
                dragForce[1]
            );
            this.applyForce(
                wheel.x,
                wheel.y,
                pForce[0],
                pForce[1]
            );
             this.applyForce(
                wheel.x,
                wheel.y,
                bForce[0],
                bForce[1]
            );
            this.applyForce(
                wheel.x, 
                wheel.y,
                accForce[0], accForce[1]
            );*/
            this.applyForce(
                wheel.x,
                wheel.y,
                totalForce[0],
                totalForce[1]
            );

            
        }
    },
    destroy: function(){
        this.finishCallback(null, null, true);
    },
    handleBodyCollisions: function(body, mtv, axis){
        if(body.peneality !== undefined && body.penealityActive){
            body.penealitize();
            this.timer.addPeneality(body.peneality);
            if(this.penealityCallback){
                this.penealityCallback();
            }else{
                this.penealitiesToDisplay.push(body.peneality);
            }
        }
    }
});
