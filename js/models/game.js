var Game = Model.extend({
    defaults: function() {
        return {
            'tickHandlers': [],
            'entities': [],
            'trackName': null,
            'carName': null,
            'carColor': null,
            'penealityCallback': null,
            'shadowData': null
        };
    },
    initialize: function(){
        if(window.loadQueue === undefined){
            this.loadQueue = new createjs.LoadQueue();
        }else{
            this.loadQueue = window.loadQueue;
        }
        //this.metadataLoaded = _.bind(this._metadataLoaded, this);
        //this.mapLoaded = _.bind(this._mapLoaded, this);
    },
    _metadataLoaded: function(){
        this.mapData = this.loadQueue.getResult("maps/"+this.mapId+"/metadata.json");
        this.loadQueue.removeEventListener("complete", this.metadataLoaded);
        this.loadQueue.addEventListener("complete", this.mapLoaded);
        this.loadQueue.loadManifest([
            "maps/"+this.mapId+"/"+this.mapData.files.colormap,
            "maps/"+this.mapId+"/"+this.mapData.files.background,
            "maps/"+this.mapId+"/"+this.mapData.files.textured
        ]);
    },
    _mapLoaded: function(){
        this.initMap();
        if(this.mapLoadedCallback !== undefined){
            this.mapLoadedCallback();
        }
        this.loadQueue.removeEventListener("complete", this.mapLoaded);
    },
    loadMap: function(mapId, callback){
        this.mapId = mapId;
        this.mapLoadedCallback = callback;
        var loadResult = this.loadQueue.getResult("maps/"+this.mapId+"/metadata.json");
        if(loadResult === null || loadResult === undefined){
            this.loadQueue.addEventListener("complete", this.metadataLoaded);
        }else{
            this.metadataLoaded();
        }
    },
    setLaps: function(laps){
        this.laps = laps;
    },
    setTrack: function(name){
        this.trackName = name;
    },
    setShadowData: function(data){
        this.shadowData = data;
    },
    setCar: function(name){
        this.carName = name;
    },
    setCarColor: function(carColor){
        this.carColor = carColor;
    },
    setPenealityCallback: function(callback){
        this.penealityCallback = callback;
    },
    setMapData: function(mapData){
        this.mapData = mapData;
    },
    initMap: function(){
        this.map = MapModel.loadFromJSON(this.mapData);
        this.map.setTrack(this.trackName);

        for(var i=0; i<this.map.objects.length; i++){
            var obj = this.map.objects[i];
            if(obj.name == "trafficCone"){
                var model = new TrafficCone({
                    'x': obj.pos[0],
                    'y': obj.pos[1],
                    'map': this.map
                });
                obj.model = model;
                this.entities.push(obj.model);
            }else{
                continue;
            }
        }

        var carsData = window.loadQueue.getResult("carsData");
        this.body = new Car(carsData[this.carName]);
        this.body.setGraphics(this.carColor);
        this.body.map = this.map;
        
        var start = this.map.getStart();
        this.body.x = utils.px2m(start.pos[0]);
        this.body.y = utils.px2m(start.pos[1]);
        this.body.r = utils.deg2rad(start.rotation);
        this.body.maxLap = this.laps;
        //this.body.finishCallback = _.bind(this.finish, this);
        var self = this;
        this.body.finishCallback = function(timer,log, destroy){
            if(destroy){
                //this is a work-around to free memory of body
                //when body is beeing destroyed it will call this fuunciton with
                //destroy = true 
                self = null; 
                return;
            }
            self.finish(timer, log);
        };
        if(this.penealityCallback){
            this.body.penealityCallback = this.penealityCallback;
        }

        if(this.shadowData !== null){
            var graphicsData = this.body.graphics[this.body.currentGraphics];
            this.shadow = new Shadow({
                image_normal: graphicsData.normal,
                image_braking: graphicsData.braking,
                data: this.shadowData
            });
            this.tickHandlers.push(this.shadow);
        }

        this.tickHandlers.push(this.body);
        this.entities.push(this.body);

        this.handleTick(1, true);
    },
    finish: function(timer,log){
        this.started = false;
        
        if(this.finishCallback){
            this.finishCallback(timer,log);
        }
    },
    setFinishCallback: function(callback){
        this.finishCallback = callback;
    },
    start: function(){
        this.started = true;
        for(var i=0; i<this.tickHandlers.length; i++){
            if(this.tickHandlers[i].start === undefined) continue;
            this.tickHandlers[i].start();
        }
    },
    handleTick: function(fps, force){
        if(!this.started && force !== true) return;
        for(var _i in this.tickHandlers){
            this._findCollisions();
            this.tickHandlers[_i].handleTick(fps);
        }
    },
    destroy: function(){
        for(var i=0; i<this.entities.length; i++){
            if(this.entities[i].destroy!==undefined) this.entities[i].destroy();
        }
        this.entities = [];
        this.tickHandlers = [];
        this.map = null;
        this.body = null;
    },
    _findCollisions: function(){
        for(var _i = 0; _i < this.entities.length; _i++){
            var a = this.entities[_i];
            for(var _j = _i+1; _j < this.entities.length; _j++){
                var b = this.entities[_j];
                var collisionData = this._testCollision(a,b);
                if(collisionData){
                    a.handleBodyCollisions(b, collisionData[0], collisionData[1]);
                    b.handleBodyCollisions(a, 
                            [-collisionData[0][0], -collisionData[0][1]],
                            [-collisionData[1][0], -collisionData[1][1]]
                    );
                }
            }
        }
    },
    _testCollision: function(a,b){
        if(a.name == "TrafficCone" && b.name == "TrafficCone") return false;
        if(a.name == "TrafficCone"){
            //swap
            c = a;
            a = b;
            b = c;
        }
        //a - car, b - traffic cone
        
        var vap = [
            a.getPointPosition(0  -a.regX, 0  -a.regY),
            a.getPointPosition(a.w-a.regX, 0  -a.regY),
            a.getPointPosition(a.w-a.regX, a.h-a.regY),
            a.getPointPosition(0  -a.regX, a.h-a.regY)
        ];
        var va = [
            utils.rotate(vap[0][0], vap[0][1], -a.r),
            utils.rotate(vap[1][0], vap[1][1], -a.r),
            utils.rotate(vap[2][0], vap[2][1], -a.r),
            utils.rotate(vap[3][0], vap[3][1], -a.r)
        ];
        var vb = utils.rotate(b.x, b.y, -a.r);
        if(vb[0] > va[0][0] && vb[0] < va[1][0] && vb[1] > va[0][1] && vb[1] < va[2][1]){
            return [[0,0],[0,0]];
        }
        return false;


        
        /*** SAT  ***/
        /*
        //get vertices of each object
        var va = [
            a.getPointPosition(0  -a.regX, 0  -a.regY),
            a.getPointPosition(a.w-a.regX, 0  -a.regY),
            a.getPointPosition(a.w-a.regX, a.h-a.regY),
            a.getPointPosition(0  -a.regX, a.h-a.regY)
        ];

        var vb = [
            b.getPointPosition(0  -b.regX, 0  -b.regY),
            b.getPointPosition(b.w-b.regX, 0  -b.regY),
            b.getPointPosition(b.w-b.regX, b.h-b.regY),
            b.getPointPosition(0  -b.regX, b.h-b.regY)
        ];
        
        //axes to test
        var axes = [
            [va[0][0] - va[1][0], -va[0][1] + va[1][1]],
            [va[1][0] - va[2][0], -va[1][1] + va[2][1]],
            [vb[0][0] - vb[1][0], -vb[0][1] + vb[1][1]],
            [vb[1][0] - vb[2][0], -vb[1][1] + vb[2][1]]
        ];
        for(var _ii=0; _ii<axes.length; _ii++){
            var l = utils.vectorLength(axes[_ii][0], axes[_ii][1]);
            axes[_ii] = [axes[_ii][0]/l, axes[_ii][1]/l];
        }

        var minOverlap = Infinity;
        var minOverlapAxis = null;
        var vectorDirection = 0;

        for(var _k=0; _k<axes.length; _k++){
            //project a on an axis
            var axis = axes[_k];
            var amin = utils.dotProduct(axis[0], axis[1], va[0][0], va[0][1]);
            var amax = amin;
            var bmin = utils.dotProduct(axis[0], axis[1], vb[0][0], vb[0][1]);
            var bmax = bmin;
            var p = null;
            var overlap = null;
            for(var _ia=1; _ia<va.length; _ia++){
                p = utils.dotProduct(axis[0], axis[1], va[_ia][0], va[_ia][1]);
                amin = Math.min(amin, p);
                amax = Math.max(amax, p);
            }
            for(var _ib=1; _ib<vb.length; _ib++){
                p = utils.dotProduct(axis[0], axis[1], vb[_ib][0], vb[_ib][1]);
                bmin = Math.min(bmin, p);
                bmax = Math.max(bmax, p);
            }

            if(amin > bmax || bmin > amax){
                return false;
            }
            if(amin < bmin){
                overlap = Math.abs(amax - bmin);
                vectorDirection = -1;
            }else{
                overlap = Math.abs(bmax - amin);
                vectorDirection = 1;
            }
            if(overlap<minOverlap){
                minOverlap = overlap;
                minOverlapAxis = axis;
            }
        }
        return [
                [minOverlapAxis[0]*minOverlap*vectorDirection, 
                 minOverlapAxis[1]*minOverlap*vectorDirection],
                [minOverlapAxis[0]*vectorDirection,
                 minOverlapAxis[1]*vectorDirection]
            ];
        */
    }
});
