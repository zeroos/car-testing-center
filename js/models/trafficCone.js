var TrafficCone = RigidBody.extend({
    defaults: function() {
        var prev = RigidBody.prototype.defaults.apply(this);
        _.extend(prev, {
            'name': "TrafficCone",
            'overturned': false,
            'w': 0.5,
            'h': 0.5,
            'm': 1,
            'peneality': 1,
            'penealityActive': true
        });
        return prev;
    },
    initialize: function(){
        RigidBody.prototype.initialize.apply(this);
    },
    handleTick: function(fps){
        RigidBody.prototype.handleTick.apply(this,[fps]);
    },
    penealitize: function(){
        if(!this.overturned){
            this.overturned = true;
            this.penealityActive = false;
            setTimeout(_.bind(function(){
                this.overturned = false;
                this.penealityActive = true;
            }, this), 3000);
        }
    },
    handleBodyCollisions: function(body, mtv, axis){
        
    }
});
