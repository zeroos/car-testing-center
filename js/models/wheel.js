var WheelModel = Model.extend({
    defaults: function() {
        return {
            'x': 0,
            'y': 0,
            'r': 0,
            'w': 0.2,
            'diameter': 16,
            'steering': 0,
            'accelerate': 0,
            'kineticFrictionFactor': 1.3,
            'staticFrictionFactor': 1.3,
            'slipping': false
        };
    }
});
