var RigidBody = Model.extend({
    defaults: function(){
        return {
            'x': 10,        //[m]
            'y': 10,        //[m]
            'w': 2.5,       //[m]
            'h': 5,         //[m]
            'regX': null,   //[m]
            'regY': null,   //[m]
            'r': 0,         //[rad]
            'vx': 0,        //[m/s]
            'vy': 0,        //[m/s]
            'vr': 0,        //[rad/s]
            'm': 1000,      //[kg]
            'inertia': null,//[kg*m^2]
            'forces': [],
            'drag': 100,
            'fps': 1,
            'map': null
        };
    },
    initialize: function() {
        if(this.regX === null){
            this.regX = this.w/2;
        }
        if(this.regY === null){
            this.regY = this.h/2;
        }
        if(this.inertia === null){
            var m = this.m;
            var w = this.w;
            var h = this.h;
            this.inertia = (1/12)*m*(w*w+h*h); //cuboid
        }
    },
    /* returns a value of speed (vector length) */
    getSpeed: function() {
        var vx = this.vx;
        var vy = this.vy;
        return Math.sqrt(vx*vx + vy*vy);
    },
    /* get points position IN GLOBAL COORDINATES SYTSTEM
     * @params x,y are in object's coorrdinates sytem
     * */
    getPointPosition: function(x,y){
        var v= utils.rotate(x,y,this.r);
        return [v[0] + this.x, v[1] + this.y];
    },
    /* get speed of given point IN OBJECT'S COORDINATES SYSTEM,
     * so y would always be 0 and x would be counted based on rotation speed
     * */
    getPointSpeed: function(x,y){
        var v = utils.rotate(x,y,Math.PI/2);
        return [v[0]*this.vr,v[1]*this.vr];
    },
    /* get speed of given point in global coordinates system */
    getPointExternalSpeed: function(x,y){
        var v = this.getPointSpeed(x,y);
        return [v[0]+this.vx, v[1]+this.vy];
    },
    /* get speed vector in object's coordinates system */
    getInternalSpeed: function(){
        return utils.rotate(this.vx, this.vy, -this.r);
    },
    handleTick: function(fps) {
        if(fps !== undefined && fps >= 1){
            this.fps = fps;
        }
        this._handleForces();
        this._handleMovement();
        this._handleMapCollisions();
    },
    applyForce: function(x,y,fx,fy){
        this.forces.push([x,y,fx,fy]);
    },
    applyExternalForce: function(x,y,fx,fy){
        var o = utils.rotate(fx,fy,-this.r);
        this.applyForce(x,y,o[0],o[1]);
    },
    destroy: function(){
        this.map = null;
    },
    _handleMovement: function() {
        this.r = this.r+this.vr/this.fps;
        this.x = this.x+this.vx/this.fps;
        this.y = this.y+this.vy/this.fps;
        
        //zero
        if(this.getSpeed() + this.vr < 0.01){
            this.vx = 0;
            this.vy = 0;
            this.vr = 0;
        }

        //drag
        if(this.getSpeed() > 2){
            this.applyExternalForce(0,0,
                    this.drag*-this.vx,
                    this.drag*-this.vy
            );
        }
    },
    _handleForces: function() {
        for(var _i in this.forces){
            var _o = this.forces[_i];

            _o = utils.rotateVector(_o[0], _o[1], _o[2], _o[3], this.r);
            var x = _o[0];
            var y = _o[1];
            var fx = _o[2];
            var fy = _o[3];

            //linear movement
            var ax = fx/this.m;
            var ay = fy/this.m;

            ax /= this.fps;
            ay /= this.fps; 

            this.vx = this.vx+ax;
            this.vy = this.vy+ay;
    
            //rotation
            var M = x*fy-y*fx;
            var ar = M/this.inertia;
            this.vr = this.vr+ar;
        }
        this.forces = [];
    },
    _handleMapCollisions: function(){
        if(this.map === null) return;
        var pointsToCheck = [];
        pointsToCheck.push([-this.regX,-this.regY]);
        pointsToCheck.push([this.w-this.regX,-this.regY]);
        pointsToCheck.push([-this.regX,this.h-this.regY]);
        pointsToCheck.push([this.w-this.regX,this.h-this.regY]);

        for(var _i=0;_i<pointsToCheck.length; _i++){
            var point = pointsToCheck[_i];
            var g_point = this.getPointPosition(point[0], point[1]);
            var solid = this.map.isSolid(g_point[0], g_point[1]);
            if(solid){
                //if(solid === true){
                    this.vx *= -1;
                    this.vy *= -1;
                    this.vr *= -1;
                    this._handleMovement();
                    this._handleMovement();
                    this.vx = 0;
                    this.vy = 0;
                    this.vr = 0;
                /*
                }else{
                    var angle = solid.angle;

                    var w = utils.rotate(1,0,angle);
                    var tvValue;
                    if(this.getSpeed() !== 0){
                        tvValue = utils.dotProduct(-this.vx, this.vy, w[0], w[1]);
                    }else{
                        tvValue = 0.5;
                    }
                    var forceValue = 5000;

                    /*I thought  that MTV would have the same value as projection of speed to w, but apparently it's much greater. Because of that tvValue is multiplied by 0.1; */
                    //this.x += w[0]*tvValue*0.05;
                    //this.y += w[1]*tvValue*0.05;
                /*
                    this.applyExternalForce(point[0], point[1],
                            w[0]*forceValue, w[1]*forceValue);

                    this.vx *= 0.9;
                    this.vy *= 0.9;
                }*/
            }
        }
    },
    handleBodyCollisions: function(body, mtv, axis){
        /*
        var type;
        this.x += mtv[0];
        this.y += mtv[1];

        var thisProjSpeedV = utils.projectVector(this.vx, this.vy, axis[0], axis[1]);
        var bodyProjSpeedV = utils.projectVector(body.vx, body.vy, axis[0], axis[1]);
        var relativeV = 0;
        if(thisProjSpeedV * bodyProjSpeedV < 0){
            relativeV = utils.vectorLength(thisProjSpeedV[0], thisProjSpeedV[1]) +
                        utils.vectorLength(bodyProjSpeedV[0], bodyProjSpeedV[1]);
        }else{
            relativeV = Math.abs(utils.vectorLength(thisProjSpeedV[0], thisProjSpeedV[1]) -
                                 utils.vectorLength(bodyProjSpeedV[0], bodyProjSpeedV[1]));
        }
        
        var factor = 1500;
        var fvalue = factor * relativeV * (1 - (this.m / (this.m+body.m)));
        //var fvalue = body.m*Math.abs(utils.vectorLength(bodyProjSpeedV[0], bodyProjSpeedV[1])) + 
        //             this.m*Math.abs(utils.vectorLength(thisProjSpeedV[0], thisProjSpeedV[1]));

        //fvalue*=body.m/(this.m+body.m);
        console.log(this.overturned!==undefined?"cone":"car", fvalue);
        var force = [fvalue*axis[0],fvalue*axis[1]];
        this.applyExternalForce(0,0,force[0],force[1]);
        */
    }

});
