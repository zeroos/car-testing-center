var Checkpoint = Model.extend({
    defaults: function() {
        return {
            'x1': 0,
            'y1': 0,
            'x2': 1,
            'y2': 1,
            'active': false,
            'display': false
        };
    },
    initialize: function(){
    },
    handleTick: function(fps){
        RigidBody.prototype.handleTick.apply(this,[fps]);
    },
    handleBodyCollisions: function(body, mtv, axis){
    }
});
