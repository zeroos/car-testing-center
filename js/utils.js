
var utils = {};
_.extend(utils, {
    m2px: function(m){
        return m*20;
    },
    px2m: function(px){
        return px/20;
    },
    rad2deg: function(rad){
        return (rad*180/Math.PI)%360;
    },
    deg2rad: function(deg){
        return (deg*Math.PI/180)%(2*Math.PI);
    },
    inch2m: function(inch){
        return inch*0.0254;
    },
    rotate: function(x,y,rad){
        var cosr = Math.cos(rad);
        var sinr = Math.sin(rad);
        var newX = x*cosr - y*sinr;
        var newY = x*sinr + y*cosr;
        return [newX, newY];
    },
    rotateVector: function(x0,y0,x1,y1,rad){
        return this.rotate(x0,y0,rad).concat(this.rotate(x1,y1,rad));
    },
    dotProduct: function(x0, y0, x1, y1){
        return x0*x1 + y0*y1;
    },
    /*projects vector x0,y0 on vector x1,y1*/
    projectVector: function(x0,y0,x1,y1){
        var v = (x0*x1+y0*y1)/(x1*x1+y1*y1);
        return [v*x1,v*y1];
    },
    vectorLength: function(x,y){
        return Math.sqrt(x*x + y*y);
    },
    pointToPointDistanceSquared: function(x1,y1,x2,y2){
        return (x1-x2) * (x1-x2) + (y1-y2) * (y1-y2);
    },
    pointToSegmentDistanceSquared: function(px,py, vx, vy, wx, wy){
        var segmentL = this.pointToPointDistanceSquared(vx,vy,wx,wy);
        if(segmentL === 0){
            return this.pointToPointDistanceSquared(px,py,wx,wy);
        }
        var d = ((px - vx) * (wx - vx) + (py - vy) * (wy - vy))/segmentL;
        if(d < 0) return this.pointToPointDistanceSquared(px,py,vx,vy);
        if(d > 1) return this.pointToPointDistanceSquared(px,py,vx,vy);
        var dx = vx + d*(wx - vx);
        var dy = vy + d*(wy - vy);
        return this.pointToPointDistanceSquared(px,py,dx, dy);
    },
    stripSuffix: function(string, suffix){
        var lastIndex = string.lastIndexOf(suffix);
        if(lastIndex == -1) return string;
        return string.substr(0,lastIndex);
    },
    img2canvas: function(img){
        var canvas = document.createElement('canvas');
        canvas.width = img.width;
        canvas.height = img.height;
        canvas.getContext('2d').drawImage(img, 0, 0, img.width, img.height);
        return canvas;
    },
    createCookie: function(name, value, days){
        var expires;
        if(days === undefined) days = 30;
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        expires = "; expires="+date.toGMTString();
        document.cookie = name+"="+value+expires+"; path=/";
    },
    readCookie: function(name){
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    },
    eraseCookie: function(name){
        this.createCookie(name, "", -1);
    }
});
