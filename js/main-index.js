var Clay = Clay || {};
Clay.gameKey = "ctc";
Clay.options = {noUI: true, fail: function(){ console.log("Clay.io failed to load");}};
Clay.readyFunctions = [];
Clay.ready = function( fn ) {
    Clay.readyFunctions.push( fn );
};
//Suggestions API is broken now, but it's worth to try it in future.
//Clay.ready(function(){new Clay.Suggestions({id: "game_suggestions"});});
(function(){
    var clay = document.createElement("script");
    clay.src = ( "https:" == document.location.protocol ? "https://" : "http://" ) + "clay.io/api/api.js"; 
    var tag = document.getElementsByTagName("script")[0];
    tag.parentNode.insertBefore(clay, tag);
 })();

window.loadQueue = new createjs.LoadQueue();
var loadCompleted = function(){
    window.preloader = new PreloaderView();
    preloader.show();
    window.soundsManager = new SoundsManager();
    window.loadQueue.installPlugin(window.soundsManager);
    window.imageManager = new ImageManager(
                 window.loadQueue.getResult("spritesheet"),
                 window.loadQueue.getResult("spritesheet-data")
    );
    window.loadQueue.loadFile("js/main.js", true);
    window.loadQueue.removeEventListener("complete", loadCompleted);
};

window.loadQueue.addEventListener("complete", loadCompleted);
window.loadQueue.addEventListener("fileload", function(ev){
    var item = ev.item;
    var type = item.type;
    if(type === createjs.LoadQueue.JAVASCRIPT){
        var head = document.getElementsByTagName('head')[0];
        item.tag.src = item.src;
        head.appendChild(item.tag);
    }
});

window.loadQueue.loadManifest(loadManifest);

