var loadManifest = [
    //Vendor
    "js/vendor/easeljs-0.6.0.min.js",
    "js/vendor/lodash.min.js",
    "js/vendor/backbone-min.js",
    //JavaScript
    "js/utils.js",
    "js/models/model.js",
    "js/models/rigidBody.js",
    "js/models/car.js",
    "js/models/wheel.js",
    "js/models/map.js",
    "js/models/lapTimer.js",
    "js/models/game.js",
    "js/models/trafficCone.js",
    "js/models/checkpoint.js",
    "js/models/shadow.js",
    "js/views/checkpoint.js",
    "js/views/onScreenButtonsCarSteering.js",
    "js/views/viewport.js",
    "js/views/rigidBody.js",
    "js/views/rigidBodyGraph.js",
    "js/views/carGraph.js",
    "js/views/car.js",
    "js/views/dashboard.js",
    "js/views/hud.js",
    "js/views/map.js",
    "js/views/game.js",
    "js/views/menu.js",
    "js/views/shadow.js",
    "js/views/preloader.js",
    "js/views/trafficCone.js",
    "js/views/onScreenMessage.js",
    "js/steering/keyboardCarSteering.js",
    "js/steering/pointerCarSteering.js",
    "js/steering/onScreenButtonsCarSteering.js",
    "js/imageManager.js",
    "js/soundsManager.js",
    //Images
    {src: "img/sprites.png", id: "spritesheet"},
    {src: "img/sprites.json", id: "spritesheet-data"},
    //Data
    {src: "data/cars.json", id: "carsData"}
];

var mapLoadManifest = [
    "maps/1/metadata.json",
    "maps/2/metadata.json",
    "maps/3/metadata.json",
    "maps/4/metadata.json",
    "maps/5/metadata.json"
];
