describe('RigidBody', function(){
    var body = new RigidBody({'x':0, 'y':0, 'vx':0, 'vy':0, 'r':0, 'vr':0});
    describe('#applyForce', function(){
        describe('after applying force (100,100) to the center', function() {
            beforeEach(function(){
                body.reset({'x':0, 'y':0, 'vx':0, 'vy':0, 'r':0, 'vr':0});
                body.applyForce(0,0,100,100);
                body.handleTick();
                body.handleTick();
            });

            it('should change body position', function(){
                body.x.should.not.equal(0);
                body.y.should.not.equal(0);
            });
            it('should change body speed', function(){
                body.getSpeed().should.not.equal(0);
            });
        });
        describe('after applying force (0,100) to the center', function() {
            beforeEach(function(){
                body.reset({'x':0, 'y':0, 'vx':0, 'vy':0, 'r':0, 'vr':0});
                body.applyForce(0,0,0,100);
                body.handleTick();
            });
            it('should go straight down', function(){
                body.x.should.equal(0, 'x');
                body.y.should.not.equal(0, 'y');
            });
            it('should not change rotation', function(){
                body.r.should.equal(0, 'r');
                body.vr.should.equal(0, 'vr');
            });
        });
        describe('after applying force (34,100) to the center', function() {
            beforeEach(function(){
                body.reset({'x':0, 'y':0, 'vx':0, 'vy':0, 'r':0, 'vr':0});
                body.applyForce(0,0,34,100);
                body.handleTick();
            });
            it('should not change rotation', function(){
                body.r.should.equal(0, 'r');
                body.vr.should.equal(0, 'vr');
            });
            it('should change position', function(){
                body.x.should.not.equal(0, 'x');
                body.y.should.not.equal(0, 'y');
            });

        });
        describe('after applying two symmetrical forces (100,0)', function(){
            beforeEach(function(){
                body.reset({'x':0, 'y':0, 'vx':0, 'vy':0, 'r':0, 'vr':0});
                body.applyForce(0,100,100,0);
                body.applyForce(0,-100,100,0);
                body.handleTick();
            });
            it('should go right', function(){    
                body.x.should.be.greaterThan(0, 'x');
                body.y.should.equal(0, 'y');
            });
            it('should not change rotation', function(){
                body.r.should.equal(0, 'r');
                body.vr.should.equal(0, 'vr');
            });
        });
        describe('after applying force (0,100) to point (2,0)', function(){
            beforeEach(function(){
                body.reset({'x':0, 'y':0, 'vx':0, 'vy':0, 'r':0, 'vr':0});
                body.applyForce(2,0,0,100);
                body.handleTick();
            });
            it('should go down', function(){    
                body.x.should.equal(0, 'x');
                body.y.should.be.greaterThan(0, 'y');
            });
            it('should start rotating clockwise', function(){
                body.r.should.be.greaterThan(0, 'r');
                body.vr.should.be.greaterThan(0, 'vr');
            });
        });
        describe('after applying force (0,100) to point (-2,0)', function(){
            beforeEach(function(){
                body.reset({'x':0, 'y':0, 'vx':0, 'vy':0, 'r':0, 'vr':0});
                body.applyForce(-2,0,0,100);
                body.handleTick();
            });
            it('should go down', function(){    
                body.x.should.equal(0, 'x');
                body.y.should.be.greaterThan(0, 'y');
            });
            it('should start rotating counterclockwise', function(){
                body.r.should.be.lessThan(0, 'r');
                body.vr.should.be.lessThan(0, 'vr');
            });
        });

        describe('after applying force to a body rotated by 90 deg', function(){
            beforeEach(function(){
                body.reset({'x':0, 'y':0, 'vx':0, 'vy':0, 'r':utils.deg2rad(90), 'vr':0});
                body.applyForce(0,0,0,100);
                body.handleTick();
            });
            it('should go left', function(){
                body.x.should.be.lessThan(0);
                body.y.should.be.closeTo(0,0.000001);
            });
            it('rotation should not change', function(){
                body.r.should.equal(utils.deg2rad(90));
            });
        });
    });
    describe('#applyExternalForce', function(){
        describe('after applying force to a body rotated by 90 deg', function(){
            beforeEach(function(){
                body.reset({'x':0, 'y':0, 'vx':0, 'vy':0, 'r':utils.deg2rad(90), 'vr':0});
                body.applyExternalForce(0,0,0,100);
                body.handleTick();
            });
            it('should go down', function(){
                body.x.should.equal(0);
                body.y.should.be.greaterThan(0);
            });
            it('rotation should not change', function(){
                body.r.should.equal(utils.deg2rad(90));
            });
        });
        describe('after applying force (0,100) to point (-2,0)', function(){
            beforeEach(function(){
                body.reset({'x':0, 'y':0, 'vx':0, 'vy':0, 'r':0, 'vr':0});
                body.applyExternalForce(-2,0,0,100);
                body.handleTick();
            });
            it('should go down', function(){    
                body.x.should.equal(0, 'x');
                body.y.should.be.greaterThan(0, 'y');
            });
            it('should start rotating counterclockwise', function(){
                body.r.should.be.not.equal(0, 'r');
                body.vr.should.be.lessThan(0, 'vr');
            });
        });
    });

    describe('#getSpeed', function() {
        it('should compute speed value', function(){
            var body = new RigidBody({'vx':0, 'vy': 4});
            body.getSpeed().should.equal(4);
            body.reset({'vx':4, 'vy': 4});
            body.getSpeed().should.equal(4*Math.sqrt(2));
        });
    });
    describe('#getPointSpeed', function(){
        var body = new RigidBody({'vx':0, 'vy': 4});
        describe("without applying any forces", function(){
            beforeEach(function(){
                body.reset({'x':0, 'y':0, 'vx':0, 'vy':0, 'r':0, 'vr':0});
                body.handleTick();
            });
            it('all points should have 0 speed', function(){
                var tests = [[0,0], [1,50], [-3,4], [-4,-4]];
                for(var _i in tests){
                    var test = tests[_i];
                    var result = body.getPointSpeed(test[0], test[1]);
                    result[0].should.equal(0);
                    result[1].should.equal(0);
                }
            });
        });
        describe("when moving without rotation", function(){
            beforeEach(function(){
                body.reset({'x':0, 'y':0, 'vx':10, 'vy':10, 'r':0, 'vr':0});
                body.handleTick();
            });
            it('all points should have 0 speed', function(){
                var tests = [[0,0], [1,50], [-3,4], [-4,-4]];
                for(var _i in tests){
                    var test = tests[_i];
                    var result = body.getPointSpeed(test[0], test[1]);
                    result[0].should.equal(0);
                    result[1].should.equal(0);
                }
            });
        });
        describe("when moving with positive rotation", function(){
            beforeEach(function(){
                body.reset({'x':0, 'y':0, 'vx':10, 'vy':10, 'r':0, 'vr':5});
                body.handleTick();
            });
            it('points in the bottom should have negative vx', function(){
                var tests = [[1,50], [-43,50], [-4,4]];
                for(var _i in tests){
                    var test = tests[_i];
                    var result = body.getPointSpeed(test[0], test[1]);
                    result[0].should.be.lessThan(0);
                }
            });
            it('points in the top should have positive vx', function(){
                var tests = [[1,-50], [-43,-30], [-4,-4]];
                for(var _i in tests){
                    var test = tests[_i];
                    var result = body.getPointSpeed(test[0], test[1]);
                    result[0].should.be.greaterThan(0);
                }
            });
            it('points in the right half should have positive vy', function(){
                var tests = [[1,-50], [43,-30], [4,-4]];
                for(var _i in tests){
                    var test = tests[_i];
                    var result = body.getPointSpeed(test[0], test[1]);
                    result[1].should.be.greaterThan(0);
                }
            });
            it('point 0,0 should have speed (0,0)', function(){
                body.getPointSpeed(0,0).should.deep.equal([0,0]);
            });
        });
        describe("when moving with negative rotation", function(){
            beforeEach(function(){
                body.reset({'x':0, 'y':0, 'vx':10, 'vy':10, 'r':0, 'vr':-5});
                body.handleTick();
            });
            it('points in the bottom should have positive vx', function(){
                var tests = [[1,50], [-43,50], [-4,4]];
                for(var _i in tests){
                    var test = tests[_i];
                    var result = body.getPointSpeed(test[0], test[1]);
                    result[0].should.be.greaterThan(0);
                }
            });
            it('points in the top should have negative vx', function(){
                var tests = [[1,-50], [-43,-30], [-4,-4]];
                for(var _i in tests){
                    var test = tests[_i];
                    var result = body.getPointSpeed(test[0], test[1]);
                    result[0].should.be.lessThan(0);
                }
            });
            it('points in the right half should have negative vy', function(){
                var tests = [[1,-50], [43,-30], [4,-4]];
                for(var _i in tests){
                    var test = tests[_i];
                    var result = body.getPointSpeed(test[0], test[1]);
                    result[1].should.be.lessThan(0);
                }
            });
            it('point 0,0 should have speed (0,0)', function(){
                body.getPointSpeed(0,0).should.deep.equal([0,0]);
            });
        });
    });
});
