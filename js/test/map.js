describe('Map', function(){
    var mapModel = MapModel.loadFromJSON(mapData);
    describe('#loadFromJSON', function(){
        it('should load map', function(){
            mapModel.colormap.localName.should.equal('canvas');
            mapModel.textured.localName.should.equal('canvas');
        });
    });
    describe("#getColor", function(){
        it('should get correct color from given coordinates', function(){
            mapModel.getColor(utils.px2m(2),utils.px2m(2)).should.equal("#ff0000");
            mapModel.getColor(utils.px2m(100),utils.px2m(250)).should.equal("#00ff00");
            mapModel.getColor(utils.px2m(600),utils.px2m(500)).should.equal("#ff0000");
        });
        it('should check 200 points in less than a second', function(){
            var maxX = mapModel.getWidth();
            var maxY = mapModel.getHeight();
            var start = new Date().getTime();
            for(var i=0; i<200; i++){
                var x = Math.floor(Math.random()*maxX);
                var y = Math.floor(Math.random()*maxY);
                mapModel.getColor(x,y);
            }
            var end = new Date().getTime();
            (end-start).should.be.lessThan(1000);
        });
    });
});
