describe('Utils', function(){
    describe('#m2px and px2m', function(){
        it('should work for m', function(){
            utils.m2px(5).should.equal(100);
            utils.m2px(1.5).should.equal(30);
        });
        it('should work for px', function(){
            utils.px2m(5).should.equal(0.25);
            utils.px2m(1.5).should.equal(0.075);
        });
        it('applying it two times should give the same result', function(){
            var tests = [1,5,10,-43,-0.5,0.43];
            for(var _i in tests){
                var _o = tests[_i];
                utils.m2px(utils.px2m(_o)).should.be.closeTo(_o,0.000001);
                utils.px2m(utils.m2px(_o)).should.be.closeTo(_o,0.000001);
            }
        });
    });
    describe("#rad2deg and deg2rad", function(){
        it('should work for rad', function(){
            utils.rad2deg(2*Math.PI).should.equal(0, '360');
            utils.rad2deg(Math.PI).should.equal(180, '180');
            utils.rad2deg(Math.PI/2).should.equal(90, '90');
        });
        it('should work for deg', function(){
            utils.deg2rad(90).should.equal(Math.PI/2, 'PI/2');
            utils.deg2rad(720).should.equal(0, '0');
        });
        it('applying it two times should give the same result', function(){
            var tests = [1,5,10,-43,-0.5,0.43, Math.PI*3];
            for(var _i in tests){
                var _o = tests[_i];
                utils.rad2deg(utils.deg2rad(_o)).should.be.closeTo(_o%360,0.000001, 'rad2deg('+_o+')');
                utils.deg2rad(utils.rad2deg(_o)).should.be.closeTo(_o%(2*Math.PI),0.000001, 'deg2rad('+_o+')');
            }
        });
    });
    describe("#rotate", function(){
        it('should work for test cases', function(){
            var tests = [
                [[0,10,90],  [-10,0]],
                [[-1,0,180], [1,0]],
                [[1,-1,45],   [Math.sqrt(2),0]]
            ];
            for(var _i in tests){
                var _o = tests[_i];
                var t = _o[0];
                var expectation = _o[1];
                var result = utils.rotate(t[0], t[1], utils.deg2rad(t[2]));
                result[0].should.be.closeTo(expectation[0], 0.00001);
                result[1].should.be.closeTo(expectation[1], 0.00001);
            }
        });
    });
    describe("#projectVector", function(){
        it('should work for test cases', function(){
            var tests = [
                [[0,5,5,0], [0,0]],
                [[5,0,1,0], [5,0]],
                [[1,1,1,0], [1,0]],
                [[0,1,1,1], [0.5,0.5]],
                [[-1,1,1,0], [-1,0]]
            ];
            for(var _i in tests){
                var _o = tests[_i];
                var t  = _o[0];
                var result = utils.projectVector(t[0],t[1],t[2], t[3]);
                result[0].should.be.closeTo(_o[1][0], 0.00001, t.toString());
                result[1].should.be.closeTo(_o[1][1], 0.00001, t.toString());
            }
        });
    });
    describe("#vectorLength", function(){
        it('should count vector length', function(){
            utils.vectorLength(1,1).should.equal(Math.sqrt(2));
            utils.vectorLength(0,10.4).should.equal(10.4);
            utils.vectorLength(0,-5).should.equal(5);
            utils.vectorLength(-10.5,0).should.equal(10.5);
        });
    });
    describe("#stripSuffix", function(){
        it("should strip suffix from string", function(){
            utils.stripSuffix("", "").should.equal("");
            utils.stripSuffix("abecadlo", "").should.equal("abecadlo");
            utils.stripSuffix("", "abe").should.equal("");
            utils.stripSuffix("abe", "be").should.equal("a");
            utils.stripSuffix("abe", "e").should.equal("ab");
            utils.stripSuffix("abe", "fuu").should.equal("abe");
            utils.stripSuffix("doing", "doing").should.equal("");
        });
    });
    describe("#pointToPointDistanceSquared", function(){
        it("should work for test data", function(){
            var tests = [
                [[5,5,5,5], 0],
                [[0,0,1,0], 1],
                [[0,0,0,2], 4],
                [[5,10,0,0], 5*5 + 10*10]
            ];
            for(var _i in tests){
                var _o = tests[_i];
                var t = _o[0];
                var result = _o[1];
                utils.pointToPointDistanceSquared(t[0], t[1],t[2], t[3], t[4], t[5]).should.be.closeTo(result, 0.000001);
            }
        });

    }),
    describe("#pointToSegmentDistanceSquared", function(){
        it("should work for test data", function(){
            var tests = [
                [[0,0,0,0,0,0], 0],
                [[1,0,0,0,0,0], 1],
                [[3,3,0,0,10,0], 9],
                [[3,3,0,-34,0,34], 9],
                [[5,0,0,0,-1,0], 25],
                [[-5,0,0,0,1,0], 25]
            ];
            for(var _i in tests){
                var _o = tests[_i];
                var t = _o[0];
                var result = _o[1];
                utils.pointToSegmentDistanceSquared(t[0], t[1],t[2], t[3], t[4], t[5]).should.be.closeTo(result, 0.000001, t);
            }
        });
    });
});
