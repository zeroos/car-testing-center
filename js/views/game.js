var GameView = function(win, doc, mainElement){
    this.win = win;
    this.doc = doc;
    this.mainElement = mainElement;
    

    this.canvas = this.doc.createElement("canvas");
    this.stage = new createjs.Stage(this.canvas);
    OnScreenMessage.defaultStage = this.stage;
    this.views = [];
    this.viewport = new Viewport();

    var self = this;
    var lastResizeCanvasCall = 0;
    var resizeCanvasTimeout = null;
    this.resizeCanvas = function(){
        var now = new Date();
        if(self === null || self.canvas.parentElement === null){
            self=null;
            return;
        }
        if(now - lastResizeCanvasCall < 500){
            clearTimeout(resizeCanvasTimeout);
            resizeCanvasTimeout = setTimeout(self.resizeCanvas, 500 - (now - lastResizeCanvasCall));
        }
        self.canvas.width = self.win.innerWidth;
        self.canvas.height = self.win.innerHeight;
        self.viewport.width = self.canvas.width;
        self.viewport.height = self.canvas.height;
        self.render();
        lastResizeCanvasCall = new Date();
    };
};

_.extend(GameView.prototype, {
    init: function(model){
        this.views = [];
        this.model = model;

        this.canvas.style.background = 'url("'+this.model.map.backgroundUrl+'")';
        this.mapView = new MapView(this.stage, this.viewport, this.model.map);

        for(var i=0; i<this.model.map.objects.length; i++){
            var obj = this.model.map.objects[i];
            if(obj.name == "trafficCone"){
                var trafficConeView = new TrafficConeView(
                    this.stage,
                    this.viewport,
                    obj.model
                );
                this.views.push(trafficConeView);
            }
        }
       if(this.model.shadow){
            this.shadowView = new ShadowView(
                    this.stage,
                    this.viewport,
                    this.model.shadow
            );
            this.views.push(this.shadowView);
        }
 
        this.bodyView = new CarView(
                this.stage, 
                this.viewport,
                this.mapView, 
                this.model.body
        );
        this.views.push(this.bodyView);

        if(window.debug){
            this.bodyGraph = new CarGraphView(
                this.stage,
                this.model.body,
                800, 100
            );
            this.views.push(this.bodyGraph);
        }
        this.dashboard = new DashboardView(this.stage, this.model.body);
        this.hud = new HUDView(this.stage, this.model.body);
        this.pointerCarSteering = new PointerCarSteering(this.model.body, this.viewport);
        this.model.tickHandlers.push(this.pointerCarSteering);
        
        var touchdevice = false;
        if(touchdevice){
            var onScreenButtonsCarSteering = new OnScreenButtonsCarSteering(this.model.body);
            var onScreenButtonsCarSteeringView = new OnScreenButtonsCarSteeringView(this.stage, onScreenButtonsCarSteering);
            this.views.push(onScreenButtonsCarSteeringView);
            onScreenButtonsCarSteering.attach();
        }
        this.pointerCarSteering.attach();
        this.viewport.follow(this.model.body);

        this.views = this.views.concat([
            this.mapView,
            this.dashboard,
            this.hud,
            this.viewport
        ]);

    },
    render: function(){
        //this.canvas.style.backgroundPosition = -this.viewport.x + "px " + -this.viewport.y + "px";
        this.stage.update();
        for(var _i in this.views){
            this.views[_i].render();
        }
    },
    attach: function(){
        this.mainElement.appendChild(this.canvas);
        this.win.addEventListener('resize', this.resizeCanvas, false);
        this.resizeCanvas();
    },
    detachModel: function(){
        this.views = [];
        this.pointerCarSteering.detach();
        this.pointerCarSteering = null;
        this.stage.removeAllChildren();
    },
    detach: function(){
        this.win.removeEventListener('resize', this.resizeCanvas, false);
        this.mainElement.removeChild(this.canvas);
        this.resizeCanvas(); //it will remove pointer to GameView
        this.viewport.follow(null);
        this.viewport = null;
        this.views = [];
        this.pointerCarSteering.detach();
        this.pointerCarSteering = null;
        OnScreenMessage.defaultStage = null;
        this.stage.removeAllChildren();
        this.stage.enableDOMEvents(false);
        this.stage.removeAllEventListeners();
        this.stage = null;

        for(var i=0; i<soundsManager.sources.length; i++){
            soundsManager.sources[i].stop(0);
        } //just a fast work-around
        soundsManager.sources = [];
    },
    /*_resizeCanvas: _.debounce(function(){
        this.canvas.width = this.win.innerWidth;
        this.canvas.height = this.win.innerHeight;
        this.viewport.width = this.canvas.width;
        this.viewport.height = this.canvas.height;
        this.render();
        if(this.canvas.parentElement === null) this=null; //replace this with self
    }, 500),*/
    countdown: function(callback){
        var countdownSound = window.soundsManager.getSource('countdown');
        if(countdownSound){
            //if the sound has not been loaded yet, skip it
            countdownSound.start(0);
        }
        new OnScreenMessage(this.stage, "3", undefined, undefined, undefined, undefined, 300, undefined, true);
        setTimeout(function(){
            new OnScreenMessage(this.stage, "2", undefined, undefined, undefined, undefined, 300, undefined, true); 
        }, 1000);
        setTimeout(function(){
            new OnScreenMessage(this.stage, "1", undefined, undefined, undefined, undefined, 300, undefined, true); 
        }, 2000);
        setTimeout(_.bind(function(){
            new OnScreenMessage(this.stage, "Go!", undefined, undefined, undefined, undefined, 300, undefined, true); 
            this.model.start();
            if(callback) callback();
        }, this), 3000);
    }
});
