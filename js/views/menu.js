var MenuView = function(mainElement){
    this.mainElement = mainElement;

    this.generateCars();
    this.generateMaps();
    this.generateLevels();

    this.backButton = document.getElementById("back_button");
    this.backButtonOptions = document.getElementById("back_button_options");
    this.backButtonStats = document.getElementById("back_button_stats");
    this.tryAgainButtonStats = document.getElementById("try_again_button_stats");
    this.backToMapsButton = document.getElementById("back_to_maps_button");
    this.storyButton = document.getElementById("story_mode_button"); 
    this.raceModeButton = document.getElementById("race_mode_button"); 
    this.creditsButton = document.getElementById("credits_button"); 
    this.optionsButton = document.getElementById("options_button"); 
    this.raceButton = document.getElementById("race_button");
    this.selectCarButton = document.getElementById("select_car_button");
    this.soundsVolumeInput = document.getElementById("sounds_volume_input");


    this.backButton.addEventListener("click", _.bind(this.main, this));
    this.backButtonOptions.addEventListener("click", _.bind(this.main, this));
    this.backButtonStats.addEventListener("click", _.bind(this.mapsMenu, this));
    this.backToMapsButton.addEventListener("click", _.bind(this.mapsMenu, this));
    this.storyButton.addEventListener("click", _.bind(this.storyMode, this));
    this.raceModeButton.addEventListener("click", _.bind(this.mapsMenu, this));
    this.creditsButton.addEventListener("click", _.bind(this.creditsMenu, this));
    this.optionsButton.addEventListener("click", _.bind(this.optionsMenu, this));
    this.raceButton.addEventListener("click", _.bind(this.startRace, this));
    this.selectCarButton.addEventListener("click", _.bind(this.carsMenu, this));
    this.soundsVolumeInput.addEventListener("change", _.bind(this.updateSoundsVolume, this));

    document.getElementById("selected_car_select").addEventListener("change", _.bind(this.carSelection, this));
    document.getElementById("selected_map_select").addEventListener("change", _.bind(this.mapSelection, this));

    

    if(window.location.search.substring(1,6) == "debug"){
        window.debug = true;
    }
    if(window.location.hash == "#story_mode"){
        this.storyMode();
        window.preloader.hide();
    }else if(window.location.hash == "#race_mode"){
        this.raceMode();
        window.preloader.hide();
    }else if(window.location.hash == "#credits"){
        this.creditsMenu();
        window.preloader.hide();
    }else if(window.location.hash == "#options"){
        this.optionsMenu();
        window.preloader.hide();
    }else if(window.location.hash.substr(1,3) == "map"){
        this.raceMode();
        window.preloader.hide();
    }else if(window.location.hash.substr(1,9) == "challenge"){
        var hash = window.location.hash;
        this.challenge(hash.substring(11,hash.length));
        window.preloader.hide();
    }else if(window.location.hash == "#race"){
        this.raceMode();
        window.preloader.hide();
    }else{
        this.main();
        window.preloader.hide();
    }


    var savedVolume = utils.readCookie("soundsVolume");
    if(savedVolume){
        this.soundsVolumeInput.value = savedVolume;
        this.updateSoundsVolume();
    }

    this.initSteeringInfo();
    this.game = null;
    this.challengeUrl = null;
    this.challengeNickname = null;
};

_.extend(MenuView.prototype, {
    main: function(){
        this.hideAll();
        document.getElementById("main_menu").style.display = "block";
    },
    storyMode: function(){
        this.generateLevels();
        this.hideAll();
        this.backButton.style.display = "block";
        Clay.ready(function(){Clay.Stats.logStat({menu: "story_mode"});});
        document.getElementById("story_menu").style.display = "block";
    },
    raceMode: function(){
        Clay.ready(function(){Clay.Stats.logStat({menu: "race_mode"});});
        return this.mapsMenu();
    },
    mapsMenu: function(){
        this.hideAll();
        this.backButton.style.display = "block";
        document.getElementById("maps_menu").style.display = "block";
    },
    optionsMenu: function(){
        this.hideAll();
        this.backButton.style.display = "block";
        Clay.ready(function(){Clay.Stats.logStat({menu: "options"});});
        document.getElementById("options_menu").style.display = "block";
    },
    creditsMenu: function(){
        this.hideAll();
        this.backButton.style.display = "block";
        Clay.ready(function(){Clay.Stats.logStat({menu: "credits"});});
        document.getElementById("credits_menu").style.display = "block";
    },
    carsMenu: function(){
        this.hideAll();
        this.backToMapsButton.style.display = "block";
        document.getElementById("cars_menu").style.display = "block";
    },
    statsMenu: function(){
        this.hideAll();
        document.getElementById("stats_menu").style.display = "block";
    },
    challenge: function(url){
        this.hideAll();
        var text = "<h2>Challenge</h2>";
        text += "<p>You have been challenged by someone for a race!</p><p>Tell us your name: <input id='challenge_nickname_input'></p>";
        this.displayPopup(text,
                [
                 ["Race!", _.bind(this.genLoadChallengeFunction(url), this), "#"],
                 ["Back to menu", _.bind(this.main, this), "#"]
                ]);


    },
    genLoadChallengeFunction: function(url){
        return function(){
            window.preloader.show();
            var xmlHttp = new XMLHttpRequest();
            xmlHttp.withCredentials = true;
            xmlHttp.open("GET", BACKEND_HOST + "racelog/get_data/" + url, false);
            xmlHttp.send();
            var data = JSON.parse(xmlHttp.responseText);
            var challengeNickname = document.getElementById("challenge_nickname_input").value;

            if(challengeNickname === "" || challengeNickname.length > 20){
                window.preloader.hide();
                window.alert("Nickname may not be empty and may not be longer than 20 characters.");
                return this.challenge(url);
            }
            this.startChallenge(url, challengeNickname, data);
        };
    },
    startChallenge: function(url, challengeNickname, data){
            this.challengeNickname = challengeNickname;
            this.challengeUrl = url;
            this.challengeData = data;
            
            var carName = data.car_name;
            var carColor = data.car_color;
            var mapId = this.mapName2Id(data.map_name);
            var trackName = data.track_name;
            var laps = data.laps;
            var positionLog = data.position_log;

            this.startGame(carName, carColor, mapId, trackName, laps, null, _.bind(this.finish, this), positionLog);
    },
    displaySteeringInfo: function(callback, force){
        if(force === undefined) force = false;
        if(callback === undefined) callback = this.mainMenu;

        if(!force){
            if(utils.readCookie("steering_displayed") === "true"){
                return callback();
            }
        }
        utils.createCookie("steering_displayed", "true");
        var preloaderOn = false;
        if(window.preloader.visible){
            preloaderOn = true;
            window.preloader.hide();
        }


        var div = document.getElementById("steering_info");
        this.hideAll();
        document.getElementById("steering_info_1").style.display = "block";
        document.getElementById("steering_info_2").style.display = "none";
        document.getElementById("steering_info_3").style.display = "none";
        div.style.display = "block";

        
        var elements = div.getElementsByClassName("steering_start");
        for(var i=0; i<elements.length; i++){
            var el = elements[i];
            el.onclick = _.bind(function(){
                this.hideAll();
                if(preloaderOn) window.preloader.show();
                callback();
            }, this);
        }
    },
    hideAll: function(){
        this.backButton.style.display = "none";
        this.backToMapsButton.style.display = "none";
        document.getElementById("maps_menu").style.display = "none";
        document.getElementById("story_menu").style.display = "none";
        document.getElementById("credits_menu").style.display = "none";
        document.getElementById("cars_menu").style.display = "none";
        document.getElementById("options_menu").style.display = "none";
        document.getElementById("main_menu").style.display = "none";
        document.getElementById("stats_menu").style.display = "none";
        document.getElementById("overlay").style.display = "none";
        document.getElementById("steering_info").style.display = "none";
    },
    updateSoundsVolume: function(){
        var volume = this.soundsVolumeInput.value;
        window.soundsManager.setSoundsVolume(volume/100);
        utils.createCookie("soundsVolume", volume);
    },
    carSelection: function(ev){
        this.refreshCarColorsList();
    },
    mapSelection: function(ev){
        this.refreshMapData();
    },
    initSteeringInfo: function(){
        var div = document.getElementById("steering_info");
        var anchors = div.getElementsByClassName("steering_button");
        for(var i=0;i<anchors.length; i++){
            var a = anchors[i];
            a.addEventListener("click", function(){
                if(this.dataset.show_id === undefined) return;
                document.getElementById(this.dataset.show_id).style.display = "block";
                this.parentElement.style.display = "none";
            });
        }

    },
    refreshCarColorsList: function(){
        var carsData = window.loadQueue.getResult("carsData");
        var colorsSelect = document.getElementById("selected_color_select");
        colorsSelect.innerHTML = "";
        document.getElementById("selected_car_name").innerHTML = this.selectedCar();
        for(var color in carsData[this.selectedCar()].graphics){
            var el = document.createElement("option");
            el.value = color;
            el.textContent = color;
            colorsSelect.appendChild(el);
        }
        var power = carsData[this.selectedCar()].maxAcceleration;
        var weight = carsData[this.selectedCar()].m;
        var drive_text = carsData[this.selectedCar()].description.drive;
        var description = carsData[this.selectedCar()].description.text;

        document.getElementById("power_progressbar").value = power;
        document.getElementById("weight_progressbar").value = weight;
        document.getElementById("drive_text").innerText = drive_text;
        document.getElementById("description_text").innerText = description;
    },
    refreshMapData: function(){
        var tracksSelect = document.getElementById("selected_track_select");
        tracksSelect.innerHTML = "";
        var currentMap = this.mapsData[this.selectedMap()];
        for(var _i in currentMap.tracks){
            var track = currentMap.tracks[_i];
            var el = document.createElement("option");
            el.value = track;
            el.textContent = track;
            tracksSelect.appendChild(el);
        }
        document.getElementById("selected_map_name").innerText = currentMap.name;
        document.getElementById("selected_map_image").src = currentMap.img;
    },
    selectedLaps: function(){
        return document.getElementById("selected_laps_select").value;
    },
    selectedMapName: function(){
        return document.getElementById("selected_map_name").innerText;
    },
    selectedMap: function(){
        return document.getElementById("selected_map_select").value;
    },
    selectedTrack: function(){
        return document.getElementById("selected_track_select").value;
    },
    selectedCar: function(){
        return document.getElementById("selected_car_select").value;
    },
    selectedColor: function(){
        return document.getElementById("selected_color_select").value;
    },
    displayPopup: function(message, buttons){
        var popup = document.getElementById("popup");
        popup.innerHTML = message;
        document.getElementById("overlay").style.display = "block"; 
        for(var i=0; i<buttons.length; i++){
            var button = document.createElement("a");
            if(buttons[i][2] !== undefined){
                button.href = buttons[i][2];
            }else{
                button.href = "#";
            }
            button.className = "menuButton";
            button.textContent = buttons[i][0];
            button.addEventListener("click", buttons[i][1]);
    
            popup.appendChild(button);
        }

    },
    levelButtonClicked: function(ev){
        var levelId = ev.target.dataset.id;
        var level = this.levels[levelId];

        var text = "<h2>Level " + levelId + "</h2>";
        text += level.message;
        this.displayPopup(text, [["Start level!", _.bind(this.genStartLevelFunction(levelId), this), "#story_mode"]]);
    },
    levelFailed: function(levelId){
        var text = "<h2>Level failed :(</h2>";
        text += "<p>But don't worry! You can always try again! ;)</p>";
        this.displayPopup(text,
                [
                 ["Try again!", _.bind(this.genStartLevelFunction(levelId), this), "#story_mode"],
                 ["Back to menu", _.bind(this.storyMode, this), "#story_mode"]
                ]);
    },
    genStartLevelFunction: function(levelId){
        var level = this.levels[levelId];
        MapModel.loadMap(level.mapId); //start preloading map

        return function(){
            this.hideAll();
            window.preloader.show();
            var game = new Game();
            game.setCar(level.carName);
            game.setCarColor(level.carColor);
            game.setTrack(level.trackName);
            game.setFinishCallback(_.bind(this.genLevelFinishedFunction(levelId), this));
            if(level.fatalPeneality){
                game.setPenealityCallback(_.bind(this.genLevelPenealityFunction(levelId), this));
            }
            game.setLaps(level.laps);
            
            var timeoutFunction = _.bind(function(){
                if(level.timeout){
                    var timeoutFunction = _.bind(function(){
                        if(this.levelFailedFlag) return;
                        if(this.levelSucceedFlag) return;
                        new OnScreenMessage(undefined, "It takes much too long...", undefined, undefined, "#f00");
                        this.levelFailedFlag = true;

                        setTimeout(_.bind(function(){
                            this.destroyGame();
                            this.levelFailed(levelId);
                        }, this), 3000);
                    }, this);
                    
                    this.levelTimeout = setTimeout(timeoutFunction, level.timeout);
                }
            }, this);
            Clay.ready(function(){Clay.Stats.level({action: "start", level: levelId});});

            this.game = game;
            
            MapModel.loadMap(level.mapId); //preload map
            this.displaySteeringInfo(_.bind(function(){
                MapModel.loadMap(
                    level.mapId,
                    _.bind(
                        this.getInitGameViewFunction(timeoutFunction), 
                    this)
                );
            },this));
        };
    },
    genLevelPenealityFunction: function(levelId){
        var called = false;
        return function(){
            if(called) return;
            if(this.levelSucceededFlag) return;
            if(this.levelFailedFlag) return;
            called = true;
            this.levelFailedFlag = true;
            Clay.ready(function(){Clay.Stats.level({action: "fail", level: levelId});});
            new OnScreenMessage(undefined, "No, no, no, you cannot hit traffic cones...", undefined, undefined, "#f00");
            setTimeout(_.bind(function(){
                this.destroyGame();
                this.levelFailed(levelId);
            }, this), 3000);
        };
    },
    genLevelFinishedFunction: function(levelId){
        return function(){
            if(this.levelFailedFlag) return;
            this.levelSucceedFlag = true;
            this.destroyGame();
            console.log("Finished level " + levelId);
            var levelPassed = utils.readCookie("level_passed");
            if(levelPassed === null || parseInt(levelPassed, 10) < levelId){
                utils.createCookie("level_passed", levelId);
            }

            var text = "<h2>You did it!</h2>";
            text += "<p>Congratulations! You did it!</p>";
            this.displayPopup(text, [["Back",_.bind(this.storyMode, this), "#story_mode"]]);
            Clay.ready(function(){Clay.Stats.level({action: "pass", level: levelId});});
            Clay.ready(function(){(new Clay.Achievement({id:"level"+levelId})).award();});
        };
    },
    destroyGame: function(){
        clearTimeout(this.levelTimeout);
        createjs.Ticker.removeAllEventListeners("tick");

        //give 100ms for all tickhandlers to finish
        setTimeout(_.bind(function(){
            this.gameView.detach();
            this.gameView = null;
            this.game.started = false;
            this.game.destroy();
            this.game = null;
            this.levelFailedFlag = undefined;
            this.levelSucceedFlag = undefined;
            OnScreenMessage.defaultStage = null;
            window.loadQueue.removeAllEventListeners();
            console.log("Destroyed game");
        }, this), 100);
    },
    startRace: function(){
        var carName = this.selectedCar();
        var carColor = this.selectedColor();
        var mapId = this.selectedMap();
        var trackName = this.selectedTrack();
        var laps = this.selectedLaps();
        var mapName = this.selectedMapName();

        //Clay.ready(function(){Clay.Stats.level({action: "start", level: mapName+"|"+trackName+"|"+laps+"|"+carName});});
        this.startGame(carName, carColor, mapId, trackName, laps, null, _.bind(this.finish, this));
    },
    startGame: function(carName, carColor, mapId, trackName, laps, startCallback, finishCallback, shadowData){
        this.hideAll();
        window.preloader.show();
        this.lastGame = {
            "carName": carName,
            "carColor": carColor,
            "mapId": mapId,
            "trackName": trackName,
            "laps": laps
        };
        var game = new Game();
        game.setCar(carName);
        game.setCarColor(carColor);
        game.setTrack(trackName);
        game.setFinishCallback(finishCallback);
        game.setLaps(laps);

        if(shadowData !== undefined){
            game.setShadowData(shadowData);
        }
        this.game = game;
        this.displaySteeringInfo(_.bind(function(){
            MapModel.loadMap(
                mapId,
                _.bind(
                    this.getInitGameViewFunction(),
                this)
            );
        },this));
    },
    getInitGameViewFunction: function(callback){
        return function(mapData){
            this.game.setMapData(mapData);
            this.game.initMap();
            var gameView = new GameView(window,document,document.getElementById('body'));
            gameView.attach();
            gameView.init(this.game);
            this.gameView = gameView;
            createjs.Ticker.addEventListener("tick", _.bind(function(){
                this.game.handleTick(createjs.Ticker.getMeasuredFPS());
                gameView.render();
            },this));
            setTimeout(function(){
                window.preloader.hide();
                gameView.countdown(callback);
            }, 500);
        };
    },
    finish: function(timer, position_log){
        var carName = this.lastGame.carName;
        var carColor = this.lastGame.carColor;
        var mapId = this.lastGame.mapId;
        var trackName = this.lastGame.trackName;
        var laps = this.lastGame.laps;
        var mapName = this.mapsData[mapId].name;

        this.destroyGame();
        //Clay.ready(function(){Clay.Stats.level({action: "pass", level: mapName+"|"+trackName+"|"+laps+"|"+carName});});
        //this.gameView.detach();
        //this.gameView = null;
        var text = "<h2>Stats:</h2>";
        text += "<ol>";
        text += "<li class='race'><label>Race:</label>";
        text += "<span class='time'>"+timer.formatTime(timer.getRaceTime()) + "</span>";
        text += "</li>";
        for(var i=0; i<timer.lapsHistory.length; i++){
            if(timer.lapsHistory[i] == timer.getBestLapTime()){
                text += "<li class='lap bestLap'><label>Lap " + (i+1) + ":</label>";
            }else{
                text += "<li class='lap'><label>Lap " + (i+1) + ":</label>";
            }
            text += "<span class='time'>" + timer.formatTime(timer.lapsHistory[i]) + "</span>";
            text += "<ol>";
            var checkpoints = timer.checkpointsHistory[i];
            for(var j=0; j<checkpoints.length; j++){
                text += "<li><label>Checkpoint " + (j+1) + ":</label>";
                text += "<span class='time'>" + timer.formatTime(checkpoints[j]) + "</span>";
                text += "</li>";
            }
            text += "</ol>";
            text += "</li>";
        }
        text += "</ol>";
        document.getElementById("stats_text").innerHTML = text;

        var formData = {
            "id_car": carName,
            "id_car_color": carColor,
            "id_track_name": trackName,
            "id_map": mapName,
            "id_position_log": JSON.stringify(position_log),
            "id_time_log": JSON.stringify(timer.lapsHistory),
            "id_laps": laps,
            "id_race_time": timer.getRaceTime()/1000,
            "id_best_lap_time": timer.getBestLapTime()/1000
        };
        if(this.challengeUrl){
            formData.id_nickname = this.challengeNickname;
            formData.id_challenge = this.challengeUrl;
        }
        for(var id in formData){
            if(document.getElementById(id) !== null){
                document.getElementById(id).value = formData[id];
            }
        }
        if(this.challengeUrl){
            document.getElementById("submit_racelog_add_form").click();
        }
        var clonedChallengeUrl = this.challengeUrl;
        var clonedChallengeNickname = this.challengeNickname;
        document.getElementById("try_again_button_stats").onclick = _.bind(function(){
            if(clonedChallengeUrl !== null){
                this.startChallenge(clonedChallengeUrl, clonedChallengeNickname, this.challengeData);
            }else{
                this.startRace();
            }
        }, this);
        this.challengeUrl = null;
        this.challengeNickname = null;

        this.statsMenu();
    },
    mapName2Id: function(name){
        for(var id in this.mapsData){
            if(this.mapsData[id].name == name) return id;
        }
        return false;
    },
    generateMaps: function(){
        this.mapsData = {
            1: {
                "name": "Near the village",
                "img": "maps/1/image.png",
                "tracks": ["Main circle", "Main circle-reverse"]
            },
            2: {
                "name": "Dark forest",
                "img": "maps/2/image.png",
                "tracks": ["Follow the road", "Follow the road-reverse", "Slalom"]
            },
            3: {
                "name": "Test Center",
                "img": "maps/3/image.png",
                "tracks": ["Sightseeing", "Snowman"]
            }, 
            4: {
                "name": "Eight",
                "img": "maps/4/image.png",
                "tracks": ["Eight", "Zero"]
            },
            5: {
                "name": "Frozen lake",
                "img": "maps/5/image.png",
                "tracks": ["Around the lake", "Around the lake-reverse", "Through ice and snow", "Slalom"]
            }
        };
        var mapsSelect = document.getElementById("selected_map_select");
        for(var mapId in this.mapsData){
            var map = this.mapsData[mapId];
            var el = document.createElement("option");
            el.value = mapId;
            el.textContent = map.name;
            mapsSelect.appendChild(el);
        }
        this.refreshMapData();
    },
    generateCars: function(){
        var carsData = window.loadQueue.getResult("carsData");
        var carsSelect = document.getElementById("selected_car_select");
        for(var carName in carsData){
            if(carsData[carName].visible === false) continue;
            var el = document.createElement("option");
            el.value = carName;
            el.textContent = carName;
            carsSelect.appendChild(el);
        }
        this.refreshCarColorsList();
    },
    generateLevels: function(){
        this.chapters = [
            {"title": "Back to driving school", "levels": 4},
            {"title": "Advanced driving test", "levels": 8}
            //{"title": "Testing cars", "levels": 4}
        ];
        this.levels = {
            "1": {
                "carName": "Fores",
                "carColor": "red",
                "mapId": 3,
                "trackName": "StartStop-easy",
                "laps": 1,
                "timeout": 10000,
                "fatalPeneality": true,
                "message": "<p>In this level your goal is to start your car, drive straight and stop in front of traffic cones. Be carefull not to touch them!</p>"
            },
            "2": {
                "carName": "Fores",
                "carColor": "black",
                "mapId": 3,
                "trackName": "Arch",
                "laps": 1,
                "timeout": 10000,//optional
                "fatalPeneality": true,
                "message": "<p>Ok, so now it's time to test your turning skill's. Can you drive along this arch without touching any traffic cones?</p>"
            },
            "3": {
                "carName": "Papar",
                "carColor": "black",
                "mapId": 3,
                "trackName": "Sliding pane",
                "laps": 1,
                "timeout": 11000,//optional
                "fatalPeneality": true,
                "message": "<p>Let's see how you can handle more slippery surfaces...</p>"
            },
            "4": {
                "carName": "Papar",
                "carColor": "choco",
                "mapId": 3,
                "trackName": "Slalom",
                "laps": 1,
                "timeout": 20000,//optional
                "fatalPeneality": true,
                "message": "<p>This time you have to be very agile. Can you pass this slalom? Remember not to touch any traffic cones!</p>"
            },
            "5": {
                "carName": "Pengus",
                "carColor": "solid-black",
                "mapId": 3,
                "trackName": "StartStop-hard",
                "laps": 1,
                "timeout": 8000,//optional
                "fatalPeneality": true,
                "message": "<p>I see you're doing pretty good out there... Let's try something a little bit harder right now. We've got a new car, pretty fast one. Check it's acceleration for me, please. Just drive straight and stop in front of traffic cones.</p>"
            },
            "6": {
                "carName": "Pengus",
                "carColor": "blue",
                "mapId": 3,
                "trackName": "Sliding pane 2",
                "laps": 1,
                "timeout": 9000,//optional
                "fatalPeneality": true,
                "message": "<p>Again, let's test your skills on the slippery surface. But this time I'd like to see some real drifts!</p>"
            },
            "7": {
                "carName": "Fores",
                "carColor": "pink",
                "mapId": 5,
                "trackName": "Through ice and snow",
                "laps": 3,
                "timeout": 118000,//optional
                "fatalPeneality": false,
                "message": "<p>Ok, let's see how you perform on the real road now. Here, take this 4WD car and hit some snowy roads.</p>"
            },
            "8": {
                "carName": "Fores-trolleys",
                "carColor": "testcar",
                "mapId": 3,
                "trackName": "Slalom-trolleys",
                "laps": 1,
                "timeout": 35000,//optional
                "fatalPeneality": true,
                "message": "<p>You're doing amazingly well! So we prepared something special for you. Try to pass the slalom again, but this time with trolleys. Don't hurry, take your time to learn how to handle them.</p>"
            },
            "9": {
                "carName": "Fores",
                "carColor": "green",
                "mapId": 3,
                "trackName": "Arch-reverse",
                "laps": 1,
                "timeout": 16000,//optional
                "fatalPeneality": true,
                "message": "<p>You did manage to drive through our arch before, but could you do it again, on reverse?</p>"

            },
            "10": {
                "carName": "Fores-trolleys",
                "carColor": "testcar",
                "mapId": 3,
                "trackName": "Slalom",
                "laps": 1,
                "timeout": 25000,//optional
                "fatalPeneality": true,
                "message": "<p>Let's try to pass slalom one more time, but a little bit faster. Remember that you have trolleys again!</p>"
            },
            "11": {
                "carName": "Pengus",
                "carColor": "green",
                "mapId": 3,
                "trackName": "Precise",
                "laps": 1,
                "timeout": 20000,//optional
                "fatalPeneality": true,
                "message": "<p>Let's practice some precise driving... Be carefull, the car we've gave you is a real beast.</p>"
            },
            "12": {
                "carName": "Fores-trolleys",
                "carColor": "testcar",
                "mapId": 3,
                "trackName": "Sightseeing",
                "laps": 3,
                "timeout": 240000,//optional
                "fatalPeneality": true,
                "message": "<p>You've finished nearly all tasks. At the end try to drive around our Test Center. Hurry up and beware of obstacles on your way!</p>"
            },
            "template": {
                "carName": "",
                "carColor": "",
                "mapId": 0,
                "trackName": "",
                "laps": 0,
                "timeout": 10000,//optional
                "fatalPeneality": true,
                "message": "test"
            }
        };
        var storyMenu = document.getElementById("story_menu");
        var text = "";
        var levelNum = 1;
        var levelPassed = utils.readCookie("level_passed");
        if(levelPassed === null) levelPassed = 0;
        else levelPassed = parseInt(levelPassed, 10);
        for(var i=0; i<this.chapters.length; i++){
            var chapter = this.chapters[i];
            text += "<div class='chapter'>";
            text += "<h2>"+chapter.title+"</h2>";
            for(var j=0; j<chapter.levels; j++){
                if(levelPassed+1 < levelNum){
                    text += "<a href='#story_mode' class='levelButton locked' data-id='"+levelNum+"'>Level "+levelNum+"</a>";
                }else{
                    text += "<a href='#story_mode' class='levelButton levelNotLocked' data-id='"+levelNum+"'>Level "+levelNum+"</a>";
                }
                levelNum++;
            }
            text += "</div>";
        }
        storyMenu.innerHTML = text;
        var levelButtons = document.getElementsByClassName("levelNotLocked");
    
        for(var k=0; k<levelButtons.length; k++){
            levelButtons[k].addEventListener("click", _.bind(this.levelButtonClicked, this));
        }
    }
});
