var OnScreenButtonsCarSteeringView = function(stage, model){
    this.model = model;

    this.stage = stage;

    var brakePedalImage = imageManager.getSprite('brake_pedal');
    this.brakePedal = new createjs.Bitmap(brakePedalImage.img);
    this.brakePedal.sourceRect = brakePedalImage.rect;
    this.w = brakePedalImage.w;
    this.h = brakePedalImage.h;
    this.brakePedal.regX = 0;
    this.brakePedal.regY = this.h;
    this.brakePedal.scaleX = 0.75;
    this.brakePedal.scaleY = 0.75;

    this.stage.addChild(this.brakePedal);
};

_.extend(OnScreenButtonsCarSteeringView.prototype, {
    render: function() {
        this.brakePedal.x = 20;
        if(this.model.brakePedalDown){
            this.brakePedal.y = stage.canvas.height*6/7+10;
        }else{
            this.brakePedal.y = stage.canvas.height*6/7;
        }
        this.model.updateBrakePedalCoordinates(
            this.brakePedal.x-this.brakePedal.regX,
            this.brakePedal.y-this.brakePedal.regY,
            parseInt(this.w,10),
            parseInt(this.h,10)
        );
    }
});
