var HUDView = function(stage, model){
    this.model = model;

    this.stage = stage;

    var yPointer = 10;
    var xPointer = 10;
    var xPointerClocks = 220;
    var shadow = new createjs.Shadow("#000000", 0, 0, 5);

    this.lapCounter = new createjs.Text("Lap -/-", "bold 45px sans-serif", "#fff");
    this.lapCounter.x = xPointer;
    this.lapCounter.y = yPointer;
    this.lapCounter.shadow = shadow;
    yPointer += this.lapCounter.getMeasuredHeight();


    yPointer += 10;
    this.currentLapLabel = new createjs.Text("Current lap:", "bold 18px sans-serif", "#fc0");
    this.currentLapLabel.x = xPointer;
    this.currentLapLabel.y = yPointer;
    this.currentLapLabel.shadow = shadow;


    this.currentLap = new createjs.Text("0:00.00", "bold 18px sans-serif", "#fc0");
    this.currentLap.x = xPointerClocks;
    this.currentLap.y = yPointer;
    this.currentLap.textAlign = "right";
    this.currentLap.shadow = shadow;



    yPointer += this.currentLapLabel.getMeasuredHeight();
    this.previousLapLabel = new createjs.Text("", "bold 18px sans-serif", "#fff");
    this.previousLapLabel.x = xPointer;
    this.previousLapLabel.y = yPointer;
    this.previousLapLabel.text = "Previous lap:";
    this.previousLapLabel.shadow = shadow;

    this.previousLap = new createjs.Text("0:00.00", "bold 18px sans-serif", "#fff");
    this.previousLap.x = xPointerClocks;
    this.previousLap.y = yPointer;
    this.previousLap.textAlign = "right";
    this.previousLap.shadow = shadow;

    yPointer += this.previousLapLabel.getMeasuredHeight();
    this.bestLapLabel = new createjs.Text("", "bold 18px sans-serif", "#d00");
    this.bestLapLabel.x = xPointer;
    this.bestLapLabel.y = yPointer;
    this.bestLapLabel.text = "Best lap:";
    this.bestLapLabel.shadow = shadow;

    this.bestLap = new createjs.Text("0:00.00", "bold 18px sans-serif", "#d00");
    this.bestLap.x = xPointerClocks;
    this.bestLap.y = yPointer;
    this.bestLap.textAlign = "right";
    this.bestLap.shadow = shadow;




    this.container = new createjs.Container();
    this.container.addChild(this.lapCounter);
    this.container.addChild(this.currentLapLabel);
    this.container.addChild(this.previousLapLabel);
    this.container.addChild(this.bestLapLabel);
    this.container.addChild(this.currentLap);
    this.container.addChild(this.previousLap);
    this.container.addChild(this.bestLap);
    this.container.regX = 0;
    this.container.regY = 0;
    this.container.y = 0;
    this.container.x = 0;

    this.stage.addChild(this.container);
};

_.extend(HUDView.prototype, {
    render: function() {
        var maxLap = "∞";
        if(this.model.maxLap != -1){
            maxLap = this.model.maxLap;
        }
        this.lapCounter.text = "Lap " + (this.model.lap+1) + "/" + maxLap;

        this.currentLap.text = this.model.timer.formatTime(this.model.timer.getCurrentLapTime());

        this.previousLap.text = this.model.timer.formatTime(this.model.timer.getPreviousLapTime()) + "\n";
        this.bestLap.text = this.model.timer.formatTime(this.model.timer.getBestLapTime());
    }
});
