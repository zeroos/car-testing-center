var ShadowView = function(stage, viewport, model){
    this.model = model;
    this.stage = stage;
    this.viewport = viewport;
    this.object = {};

    this.carImage = imageManager.getSprite(model.image_normal);
    this.carImage_braking = imageManager.getSprite(model.image_braking);

    this.object = new createjs.Bitmap(this.carImage.img);
    this.object.sourceRect = this.carImage.rect;
    this.object.shadow = new createjs.Shadow("#000000", 2, 2, 0);
    this.object.alpha = 0.5;

    this.stage.addChild(this.object);
};

_.extend(ShadowView.prototype, {
    render: function(){
        this.x = this.model.x;
        this.y = this.model.y;
        this.object.x = utils.m2px(this.x)-this.viewport.x;
        this.object.y = utils.m2px(this.y)-this.viewport.y;
        this.object.rotation = utils.rad2deg(this.model.r);
        this.object.regX = utils.m2px(this.model.regX);
        this.object.regY = utils.m2px(this.model.regY);
    }
});
