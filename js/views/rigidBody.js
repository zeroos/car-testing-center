var RigidBodyView = function(stage, model){
    this.stage = stage;
    this.model = model;

    var w = utils.m2px(this.model.w);
    var h = utils.m2px(this.model.h);

    this.object = new createjs.Shape();
    this.object.graphics
        .setStrokeStyle(1)
        .beginStroke("#000000")
        .beginFill("#ff0000")
        .drawRect(0,0, w, h);

    this.x = this.model.x;
    this.y = this.model.y;

    this.object.cache(0,0,w,h);
    this.object.shadow = new createjs.Shadow("#000000", 2, 2, 8);

    this.stage.addChild(this.object);

};

_.extend(RigidBodyView.prototype, {
    render: function(){
        this.x = this.model.x;
        this.y = this.model.y;
        this.object.x = utils.m2px(this.x)-this.viewport.x;
        this.object.y = utils.m2px(this.y)-this.viewport.y;
        this.object.rotation = utils.rad2deg(this.model.r);
        this.object.regX = utils.m2px(this.model.regX);
        this.object.regY = utils.m2px(this.model.regY);
    }
});

