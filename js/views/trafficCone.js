var TrafficConeView = function(stage, viewport, model){
    this.model = model;
    this.stage = stage;
    this.viewport = viewport;

    this.image = imageManager.getSprite('traffic_cone');
    this.image_overturned = imageManager.getSprite('traffic_cone-overturned');
    this.object = new createjs.Bitmap(this.image.img);
    this.object.sourceRect = this.image.rect;
    this.object.shadow = new createjs.Shadow("#000000", 2, 2, 0);

    this.stage.addChild(this.object);

};

_.extend(TrafficConeView.prototype, RigidBodyView.prototype);
_.extend(TrafficConeView.prototype, {
    render: function(){
        RigidBodyView.prototype.render.apply(this);
        if(this.model.overturned){
            this.object.image = this.image_overturned.img;
            this.object.sourceRect = this.image_overturned.rect;
        }else{
            this.object.image = this.image.img;
            this.object.sourceRect = this.image.rect;
        }
    }
});
