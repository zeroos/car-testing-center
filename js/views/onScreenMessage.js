var OnScreenMessage = function(stage, text, x, y, color, font, timeout, shadow, forceOverlap){
    if(stage === undefined){
        stage = OnScreenMessage.defaultStage;
    }
    this.stage = stage;

    if(font === undefined){
        font = "40px sans-serif";
    }
    if(color === undefined){
        color = "#ffcf00";
    }
    if(shadow === undefined){
        shadow = new createjs.Shadow("#000", 0, 0, 3);
    }

    this.text = new createjs.Text(text, font, color);

    this.text.textAlign = 'center';
    this.text.textBaseline = 'middle';

    if(x === undefined || y === undefined){
        this.text.x = stage.canvas.width/2;

        if(OnScreenMessage.displayingMessage && forceOverlap !== true){
            this.text.y = (stage.canvas.height/2)+45;
        }else{
            this.text.y = stage.canvas.height/2;
        }
    }else{
        this.text.x = x;
        this.text.y = y;
    }

    if(timeout === undefined){
        timeout = 3000;
    }
    if(timeout >= 0){
        this.timeout = setTimeout(_.bind(this.startDestroyAnim, this), timeout);
    }

    //this.tick = _.bind(this._tick, this);

    this.text.shadow = shadow;
    this.stage.addChild(this.text);
    OnScreenMessage.displayingMessage = true;
};

_.extend(OnScreenMessage.prototype, {
    render: function() {
        var text = "";
    },
    setTimeout: function(timeout){
        clearTimeout(this.timeout);
        this.timeout = setTimeout(_.bind(this.startDestroyAnim, this), timeout);
    },
    startDestroyAnim: function(){
        this.text.addEventListener("tick", _.bind(this.tick, this));
    },
    tick: function(f){
        if(this.text.alpha <= 0){
            this.destroy();
        }
        this.text.scaleX *= 1.1;
        this.text.scaleY *= 1.1;
        this.text.alpha -= 0.05;
    },
    destroy: function(){
        //this.text.removeEventListener("tick", this.tick);
        this.text.removeAllEventListeners();
        this.stage.removeChild(this.text);
        OnScreenMessage.displayingMessage = false;
    }
    
});
