var PreloaderView = function(){
    this.element = document.getElementById("preloader");
    this.parentElement = this.element.parentElement;

    this.destroy = _.bind(this._destroy, this);
    this.visible = true;
    /*if(window.loadQueue !== undefined){
        window.loadQueue.addEventListener("progress", _.bind(this.progress, this));
    }*/
};

_.extend(PreloaderView.prototype, {
    show: function() {
        this.visible = true;
        this.parentElement.appendChild(this.element);
    },
    hide: function(){
        if(this.visible){
            this.element.style.opacity = 0;
            setTimeout(this.destroy, 500);
            this.visible = false;
        }
    },
    _destroy: function(){
        this.element.style.opacity = 1;
        this.parentElement.removeChild(this.element);
    },
    progress: function(ev){
        if(this.visible){
            console.log("progress", ev.loaded);
        }
    }
});
