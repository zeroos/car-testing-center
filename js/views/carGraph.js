var CarGraphView = function(stage, model, x, y, scale){
    RigidBodyGraphView.apply(this, [stage,model,x,y,scale]);
    this.wheelsGraphics = new createjs.Container();
    this.wheelsGraphics.regX = utils.m2px(this.model.regX)*this.scale;
    this.wheelsGraphics.regY = utils.m2px(this.model.regY)*this.scale;
    this.wheelsGraphics.x = x;
    this.wheelsGraphics.y = y;
    this.wheels = [];

    this.stage.addChild(this.wheelsGraphics);

        
    for(var _i in this.model.wheels){
        var wheelModel = this.model.wheels[_i];
        var wheel = new createjs.Shape();
        wheel.regX = 0;
        wheel.regY = 0;
        wheel.x = utils.m2px(wheelModel.x)*this.scale+this.wheelsGraphics.regX;
        wheel.y = utils.m2px(wheelModel.y)*this.scale+this.wheelsGraphics.regY;
        wheel.rotation = utils.rad2deg(wheelModel.r);
        this.wheels[_i] = wheel;
        this.wheelsGraphics.addChild(wheel);
        this.updateWheel(_i);
    }
};

_.extend(CarGraphView.prototype, RigidBodyGraphView.prototype);
_.extend(CarGraphView.prototype, {
    render: function(){
        var regX = this.model.regX;
        var regY = this.model.regY;
        RigidBodyGraphView.prototype.render.apply(this);
        this.wheelsGraphics.rotation = utils.rad2deg(this.model.r);
        this.wheelsGraphics.regX = utils.m2px(regX)*this.scale;
        this.wheelsGraphics.regY = utils.m2px(regY)*this.scale;
        for(var _i in this.model.wheels){
            this.updateWheel(_i);
        }
    },
    updateWheel: function(i){
        var wheelModel = this.model.wheels[i];
        var wheel = this.wheels[i];
            
        if(wheel.prevR !== wheelModel.r){
            wheel.rotation = utils.rad2deg(wheelModel.r);
        }
        if(wheel.prevS !== wheelModel.slipping){
            wheel.graphics
                .setStrokeStyle(2)
                .beginStroke("#000000");

            
            if(wheelModel.slipping){
                wheel.graphics.beginFill("#ff3333");
            }else{
                wheel.graphics.beginFill("#333333");
            }

            wheel.graphics
                .drawRoundRect(
                    -utils.m2px(wheelModel.w/2)*this.scale,
                    -utils.m2px(utils.inch2m(wheelModel.diameter)/2)*this.scale,
                    utils.m2px(wheelModel.w)*this.scale,
                    utils.m2px(utils.inch2m(wheelModel.diameter))*this.scale,
                    1
            );
            wheel.cache(//not sure if it works
                    -utils.m2px(wheelModel.w/2)*this.scale,
                    -utils.m2px(utils.inch2m(wheelModel.diameter)/2)*this.scale,
                    utils.m2px(wheelModel.w)*this.scale,
                    utils.m2px(utils.inch2m(wheelModel.diameter))*this.scale
            );
        }

        wheel.prevR = wheel.rotation;
        wheel.prevS = wheelModel.slipping;
    }
});
