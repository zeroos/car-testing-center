var Viewport = function(){
    this.x = 0;//[px]
    this.y = 0;//[px]
    this.width = 100;//[px]
    this.height = 100;//[px]
};

_.extend(Viewport.prototype, {
    follow: function(object){
        this.object = object;
    },
    render: function(){
        this.x = utils.m2px(this.object.x)-this.width/2;
        this.y = utils.m2px(this.object.y)-this.height/2;
    }
});

