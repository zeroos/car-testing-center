var MapView = function(stage, viewport, model){
    this.model = model;
    this.stage = stage;
    this.viewport = viewport;

    this.canvas = model.textured;
    this.bitmap = new createjs.Bitmap(this.canvas);
    stage.addChild(this.bitmap);

    if(window.debug){
        this.paintCheckpoints();
    }

    this.checkpointViews = [];

    for(var _i in this.model.checkpoints){
        var checkpoint = this.model.checkpoints[_i];
        if(checkpoint.display){
            var checkpointView = new CheckpointView(stage, viewport, checkpoint);
            this.checkpointViews.push(checkpointView);
        }
    }
};

_.extend(MapView.prototype, {
    render: function(){
        this.bitmap.x = -this.viewport.x;
        this.bitmap.y = -this.viewport.y;
        for(var _i in this.checkpointViews){
            this.checkpointViews[_i].render();
        }
    },
    paintSlip: function(x1,y1,x2,y2){
        var color = this.model.getSlipColor(x1,y1);
        x1 = utils.m2px(x1);
        y1 = utils.m2px(y1);
        x2 = utils.m2px(x2);
        y2 = utils.m2px(y2);
        var ctx = this.canvas.getContext('2d');
        ctx.lineWidth = 5;
        ctx.strokeStyle = color;
        ctx.beginPath();
        ctx.moveTo(x1,y1);
        ctx.lineTo(x2,y2);
        ctx.stroke();
    },
    paintCheckpoints: function(){
        for(var _i in this.model.checkpoints){
            var checkpoint = this.model.checkpoints[_i];
            this.paintCheckpoint(checkpoint);
        }
    },
    paintCheckpoint: function(checkpoint){
        x1 = utils.m2px(checkpoint.x1);
        y1 = utils.m2px(checkpoint.y1);
        x2 = utils.m2px(checkpoint.x2);
        y2 = utils.m2px(checkpoint.y2);
        var ctx = this.canvas.getContext('2d');
        ctx.lineWidth = 20;
        ctx.strokeStyle = "rgba(200,0,0,0.8)";
        ctx.lineCap = "round";
        ctx.beginPath();
        ctx.moveTo(x1,y1);
        ctx.lineTo(x2,y2);
        ctx.stroke();
    }
});
