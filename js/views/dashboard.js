var DashboardView = function(stage, model){
    this.model = model;

    this.stage = stage;

    var dashboardImage = imageManager.getSprite('dashboard');
    var dashboardCanvas = utils.img2canvas(dashboardImage.img);

    var speedometerLabels = [0,20,40,60,80,100,140];
    var speedometerLabelsPositions = [[303,245], [282,223], [266,194], [259,160], [268, 125], [285,98], [333,73]];
    this.printLabels(dashboardCanvas, speedometerLabels, speedometerLabelsPositions);
    var tachometerLabels = [0,1,2,3,4,5,6,7,8];
    var tachometerLabelsPositions = [[142,259], [120,255], [100,242], [85,225], [77, 203], [81,181], [89,161], [106,139], [127,133]];
    this.printLabels(dashboardCanvas, tachometerLabels, tachometerLabelsPositions);

    this.bg = new createjs.Bitmap(dashboardCanvas);
    this.bg.sourceRect = dashboardImage.rect;
    this.w = dashboardImage.w;
    this.h = dashboardImage.h;

    var speedometerImage = imageManager.getSprite('speedometer_needle');
    this.speedometer = new createjs.Bitmap(speedometerImage.img);
    this.speedometer.sourceRect = speedometerImage.rect;
    this.speedometer.regX = 22;
    this.speedometer.regY = 124;
    this.speedometer.x = 345;
    this.speedometer.y = 159;
    this.speedometer.minRotation = 205;


    var tachometerImage = imageManager.getSprite('tachometer_needle');
    this.tachometer = new createjs.Bitmap(tachometerImage.img);
    this.tachometer.sourceRect = tachometerImage.rect;
    this.tachometer.regX = 18;
    this.tachometer.regY = 104;
    this.tachometer.x = 141;
    this.tachometer.y = 191;
    this.tachometer.minRotation = 173;


    var smallNeedle = imageManager.getSprite('dashboard_small_needle');
    this.fuelNeedle = new createjs.Bitmap(smallNeedle.img);
    this.fuelNeedle.sourceRect = smallNeedle.rect;
    this.fuelNeedle.regX = 8;
    this.fuelNeedle.regY = 45;
    this.fuelNeedle.x = 433;
    this.fuelNeedle.y = 174;
    this.fuelNeedle.rotation = 50;
    
    this.tempNeedle = new createjs.Bitmap(smallNeedle.img);
    this.tempNeedle.sourceRect = smallNeedle.rect;
    this.tempNeedle.regX = 8;
    this.tempNeedle.regY = 45;
    this.tempNeedle.x = 400;
    this.tempNeedle.y = 109;
    this.tempNeedle.rotation = 30;



    this.slippingImage_on = imageManager.getSprite('slipping_icon-on');
    this.slippingImage_off = imageManager.getSprite('slipping_icon-off');
    this.slippingImage = new createjs.Bitmap(this.slippingImage_off.img);
    this.slippingImage.sourceRect = this.slippingImage_off.rect;
    this.slippingImage.x = 320;
    this.slippingImage.y = 210;


    this.absImage_on = imageManager.getSprite('abs_icon-on');
    this.absImage_off = imageManager.getSprite('abs_icon-off');
    this.absImage = new createjs.Bitmap(this.absImage_off.img);
    this.absImage.sourceRect = this.absImage_off.rect;
    this.absImage.x = 352;
    this.absImage.y = 210;


    this.engineImage_on = imageManager.getSprite('engine_icon-on');
    this.engineImage_off = imageManager.getSprite('engine_icon-off');
    this.engineImage = new createjs.Bitmap(this.engineImage_off.img);
    this.engineImage.sourceRect = this.engineImage_off.rect;
    this.engineImage.x = 387;
    this.engineImage.y = 210;




    this.text = new createjs.Text("", "14px sans-serif", "black");
    this.text.x = 10;
    this.text.y = 10;

    this.container = new createjs.Container();
    this.container.addChild(this.bg, this.text, this.slippingImage, this.absImage, this.engineImage, this.fuelNeedle, this.tempNeedle, this.speedometer, this.tachometer);
    this.container.regX = this.w;
    this.container.regY = this.h;
    this.container.scaleX = 0.75;
    this.container.scaleY = 0.75;

    this.stage.addChild(this.container);
};

_.extend(DashboardView.prototype, {
    printLabels: function(canvas, labels, labelsPos){
        var ctx = canvas.getContext('2d');
        for(var _i in labels){
            var label = labels[_i];
            var pos = labelsPos[_i];
            ctx.strokeStyle = "#aaaaaa";
            ctx.strokeText(label, pos[0], pos[1]);
        }
    },
    render: function() {
        this.tachometer.rotation = 173;
        this.container.y = this.stage.canvas.height;
        this.container.x = this.stage.canvas.width;
        var v = this.model.getSpeed();
        var vkm = v*3.6;
        vkm = Math.floor(vkm*100)/100;
        var vm = Math.floor(v*100)/100;
        var vr = Math.round(this.model.vr*10)/10;
        var rpm = Math.floor(this.model.rpm);
        var clutch = this.model.clutch;
        var gear = this.model.gear;
        var intendedClutch = this.model.intendedClutch;
        if(window.debug){
            this.text.text = vkm + " km/h\n" + vm + " m/s\n" + vr + " deg/s\n" + rpm + " RPM\n" + "gear: " + gear + "\nclutch: " + clutch + '\niclutch: ' + intendedClutch;
        }

        this.speedometer.rotation = this.speedometer.minRotation + (vkm*21.5)/20;

        this.tachometer.rotation = this.tachometer.minRotation + (rpm*22)/1000;

        if(this.model.slipping && (Date.now()%1000) < 500){
            this.slippingImage.image = this.slippingImage_on.img;
            this.slippingImage.sourceRect = this.slippingImage_on.rect;
        }else{
            this.slippingImage.image = this.slippingImage_off.img;
            this.slippingImage.sourceRect = this.slippingImage_off.rect;
        }
    }
});
