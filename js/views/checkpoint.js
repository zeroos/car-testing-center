var CheckpointView = function(stage, viewport, model){
    this.model = model;

    this.stage = stage;
    this.viewport = viewport;

    var imgData = imageManager.getSprite("arrow");
    this.img = new createjs.Bitmap(imgData.img);
    this.img.sourceRect = imgData.rect;
    this.w = imgData.w;
    this.h = imgData.h;
    this.img.regX = this.w/2;
    this.img.regY = this.h/2;
    this.x = utils.m2px((this.model.x1 + this.model.x2)/2);
    this.y = utils.m2px((this.model.y1 + this.model.y2)/2);

    var dx = this.model.x2-this.model.x1;
    var dy = this.model.y2-this.model.y1;

    this.img.rotation = utils.rad2deg(
        Math.atan(dy/dx)
    );
    if((dy<0 || (dy===0 && dx<0)) && dx !== 0) this.img.rotation += 180;

    this.stage.addChild(this.img);
};

_.extend(CheckpointView.prototype, {
    render: function() {
        this.img.x = this.x -this.viewport.x;
        this.img.y = this.y -this.viewport.y;
        this.img.visible = this.model.active;
    }
});
