var RigidBodyGraphView = function(stage, model, x, y, scale){
    if(scale === undefined) scale = 1.5;
    this.stage = stage;
    this.model = model;
    this.scale = scale;

    this.speedScale = 0.1;
    this.forceScale = 0.003;

    var w = utils.m2px(this.model.w*scale);
    var h = utils.m2px(this.model.h*scale);

    this.object = new createjs.Shape();
    this.object.x = x;
    this.object.y = y;
    this.object.graphics
        .setStrokeStyle(1)
        .beginStroke("#000000")
        .beginFill("#aaaaaa")
        .drawRoundRect(0,0, w, h, 10);
    this.object.cache(0,0,w+2,h+2);
    //this.object.shadow = new createjs.Shadow("#000000", 2, 2, 8);

    this.graph = new createjs.Shape();
    this.graph.x = x;
    this.graph.y = y;

    this.stage.addChild(this.object);
    this.stage.addChild(this.graph);

};

_.extend(RigidBodyGraphView.prototype, {
    render: function(){
        this.graph.graphics.clear();
        this.object.rotation = utils.rad2deg(this.model.r);
        this.object.regX = utils.m2px(this.model.regX)*this.scale;
        this.object.regY = utils.m2px(this.model.regY)*this.scale;
        this.graph.rotation = utils.rad2deg(this.model.r);
        this.graph.regX = utils.m2px(this.model.regX)*this.scale;
        this.graph.regY = utils.m2px(this.model.regY)*this.scale;


        //forces
        for(var _i in this.model.forces){
            var force = this.model.forces[_i];
            this.renderVector(force[0], force[1], 
                force[2]*this.forceScale, force[3]*this.forceScale, 
                "#dd0000"
            );
        }

        var internalSpeed = this.model.getInternalSpeed();
        this.renderVector(0,0, 
                internalSpeed[0]*this.speedScale, 
                internalSpeed[1]*this.speedScale
        );

    },
    /* This vectors are relative to global coordinates system.
     *
     * x0, y0 - starting position of the vector
     * x1, y1 - coordinates of the vector
     */
    renderExternalVector: function(x0,y0,x1,y1,color){
        var rad = -this.model.r;
        var p0 = utils.rotate(x0,y0,rad);
        var p1 = utils.rotate(x1,y1,rad);
        this.renderVector(p0[0], p0[1], p1[0], p1[1], color);
    },
    /* 
     * This vectors are relative to car's coordinates system.
     *
     * x0, y0 - starting position of the vector
     * x1, y1 - coordinates of the vector
     */
    renderVector: function(x0,y0,x1,y1,color){
        if(color === undefined) color = "#000000";
        
        startX = (x0+this.model.regX)*this.scale;
        startY = (y0+this.model.regY)*this.scale;

        this.graph.graphics
            .beginStroke(color)
            .setStrokeStyle(2)
            .moveTo(
                utils.m2px(startX), 
                utils.m2px(startY)
            ).lineTo(
                utils.m2px(startX + x1), 
                utils.m2px(startY + y1)
            ).endStroke();
    }
});

