var CarView = function(stage, viewport, mapView, model){
    this.model = model;
    this.stage = stage;
    this.mapView = mapView;
    this.viewport = viewport;

    var graphicsData = this.model.graphics[this.model.currentGraphics];
    this.carImage = imageManager.getSprite(graphicsData.normal);
    this.carImage_braking = imageManager.getSprite(graphicsData.braking);
    this.object = new createjs.Bitmap(this.carImage.img);
    this.object.sourceRect = this.carImage.rect;
    this.object.shadow = new createjs.Shadow("#000000", 2, 2, 0);

    this.stage.addChild(this.object);
    this.soundSources = [];
    this.skidAsphaltSource = null;
    this.skidGrassSource = null;
    this.windSource = null;

    this.initSound();

};

_.extend(CarView.prototype, RigidBodyView.prototype);
_.extend(CarView.prototype, {
    render: function(){
        RigidBodyView.prototype.render.apply(this);
        if(this.model.brakes > 0){
            this.object.image = this.carImage_braking.img;
            this.object.sourceRect = this.carImage_braking.rect;
        }else{
            this.object.image = this.carImage.img;
            this.object.sourceRect = this.carImage.rect;
        }

        for(var i in this.model.wheels){
            var wheel = this.model.wheels[i];
            if(wheel.slipping){
                var x = wheel.x;
                var y = wheel.y;
                var pos = this.model.getPointPosition(x,y);
                var v = this.model.getPointSpeed(x,y);
                this.mapView.paintSlip(
                    pos[0]-utils.px2m(this.model.vx-v[0]),
                    pos[1]-utils.px2m(this.model.vy-v[1]), 
                    pos[0],pos[1]
                );
            }
        }
        this._handleSounds();
        this._handlePenealities();
    },
    initSound: function(){
        if(!window.soundsManager.audioSupported) return;
        /*
         * aliencar:
        var sounds = [
            [1.1,3],
            [3,4],
            [4,6.5],
            [6.6,8.9],
            [9.1,10.35]
        ];
        */
        //var sources = [];

        for(var _i=0; _i<this.model.sounds.engine.sections.length; _i++){
            window.soundsManager.getSource(this.model.sounds.engine.fileId, _.bind(this._genAppendSoundSourceFunction(_i), this));
        }
        window.soundsManager.getSource('skid-asphalt', _.bind(function(source){
            source.loop = true;
            source.gain.value = 0;
            source.start(0);
            this.skidAsphaltSource = source;
        }, this));
        window.soundsManager.getSource('skid-grass', _.bind(function(source){
            source.loop = true;
            source.gain.value = 0;
            source.start(0);
            this.skidGrassSource = source;
        }, this));
        window.soundsManager.getSource('wind', _.bind(function(source){
            source.loop = true;
            source.gain.value = 0;
            source.start(0);
            this.windSource = source;
        }, this));

    },
    _genAppendSoundSourceFunction: function(sourceId){
        return function(source){
            source.loop = true;
            source.loopStart = this.model.sounds.engine.sections[sourceId][0];
            source.loopEnd = this.model.sounds.engine.sections[sourceId][1];
            source.gain.value = 0;
            source.start(0,this.model.sounds.engine.sections[sourceId][0]);
            this.soundSources[sourceId] = source;
        };
    },
    _handlePenealities: function(){
        var p = this.model.penealitiesToDisplay.pop();
        while(p){
            new OnScreenMessage(this.stage, "+"+p+"s", undefined, undefined, "#f00");
            p = this.model.penealitiesToDisplay.pop();
        }
    },
    _handleSounds: function(){
        if(!window.soundsManager.audioSupported) return;

        var rpm = this.model.rpm;
        rpm = rpm*7000/8000;
        for(var _i=0; _i<this.soundSources.length; _i++){
            var f= (rpm/1000)-1-_i;
            if(f<-1 || f>1){
                this.soundSources[_i].gain.value = 0;
                this.soundSources[_i].playbackRate = 1;
            }else{
                this.soundSources[_i].gain.value = (1-Math.abs(f))*0.5*Math.PI;
                this.soundSources[_i].playbackRate.value = rpm/((_i+1)*1000);
            }
        }

        if(this.skidAsphaltSource === null || this.skidGrassSource === null || this.windSource === null) return;
        var asphaltSlippingWheelsNum = 0;
        var grassSlippingWheelsNum = 0;
        var onGrassWheelsNum = 0;
        var speed = this.model.getSpeed()*3.6;
        for(var i in this.model.wheels){
            var wheel = this.model.wheels[i];
            var pos = this.model.getPointPosition(wheel.x,wheel.y);
            var pointData = this.mapView.model.getData(pos[0], pos[1]);
            if(pointData === undefined || pointData.slip === undefined) continue;
            if(wheel.slipping){
                if(pointData.slip.sound == "grass"){
                    grassSlippingWheelsNum ++;
                }else{
                    asphaltSlippingWheelsNum++;
                }
            }
            if(pointData.slip.sound == "grass"){
                onGrassWheelsNum ++;
            }
        }
        
        var onGrassGainValue = 0;
        var skidGrassGainValue = grassSlippingWheelsNum/4;
        this.skidAsphaltSource.gain.value = asphaltSlippingWheelsNum/8;
        if(speed > 60){
            onGrassGainValue = onGrassWheelsNum/4;
        }else{
            onGrassGainValue = (onGrassWheelsNum*speed/60)/4;
        }
        
        if(onGrassGainValue > skidGrassGainValue){
            this.skidGrassSource.gain.value = onGrassGainValue;
        }else{
            this.skidGrassSource.gain.value = skidGrassGainValue;
        }

        if(speed > 140){
            this.windSource.gain.value = 1;
        }else{
            this.windSource.gain.value = speed/140;
        }
    }
});
