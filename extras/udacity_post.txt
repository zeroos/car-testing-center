# Car Testing Center

![Car Testing Center][1]
![Screenshot][2]

## About
In this game you can take a role of young driver who has just applied for a job in Car Testing Center.  As an entry test your boss gives you some challenges to complete.

Additionally, you can test your driving skills on one of our carefully crafted tracks in a variety of different locations - each of them has its unique properties and needs a slightly different driving technique... So, what are you waiting for? Pick one of the cars and hit the road! Ready, set, go!

## Let's play!
You can play Car Testing Center on Clay.io (http://ctc.clay.io) or on my own server (http://ctc.mbarcis.net).


## Development details

This is my second time I've tried to implement a physics-centered driving simulation in HTML5. At the first time I've failed miserably, but I've learned a lot and it surely helped me this time.

### Physics

The car is treated as a rectangular rigid body floating around without friction. Each wheel generates some forces that act on this body. These forces depend on the environment the car is in, its speed, type, etc. That way the car behaves pretty realistically - for example if you try to turn with too much speed, front wheels will start slipping (I didn't have to program this behaviour explicite, it just works because of the physics model). 

### Libraries

I've used [EaselJS][4] and [PreloadJS][5] from [CreateJS][6] suite. I like both of them a lot - very well documented, pretty fast and easy to integrate. Now I regret I didn't use any physics library, but at the beginning I thought it would be fun to write one on my own. I did learn a lot doing that, but also wasted a lot of time debugging code with a lot of vector math in it. :)

### Graphics 

Most of the graphics in this game were made exclusively for the game. Others are in Public Domain and were found on the Internet. It was pretty tricky to coordinate the work of multiple artists in one project.

### Sounds

Most of the sounds were created by me. It was a great experience to record car's engine, cut the samples and simulate the sound of the engine in game. Some of the sounds in game are in Public Domain and were found on the Internet.

## Code
The code is public and available on Bitbucket:
https://bitbucket.org/zeroos/car-testing-center

## Udacity ID
232002@gmail.com

## License
I released the code of this game and all media associated with it (including this post and images in it) under [CC BY-NC-SA 3.0][3] license.

  [1]: http://ctc.mbarcis.net/extras/markets/banner_920x680.jpg
  [2]: http://ctc.mbarcis.net/extras/markets/screenshot1_1020x685.png
  [3]: http://creativecommons.org/licenses/by-nc-sa/3.0/
  [4]: http://www.createjs.com/#!/EaselJS
  [5]: http://www.createjs.com/#!/PreloadJS
  [6]: http://www.createjs.com/

